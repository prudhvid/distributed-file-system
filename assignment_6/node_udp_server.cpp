#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include "dirent.h"

#define MAIN_NODE_IP "127.0.0.1"
#define MAIN_NODE_PORT "8085"

using namespace std;

typedef struct{
	long mtype;
	char mtext[BUFSIZ];
}message;

unsigned long long consistent_hash(const char *p){ 
    unsigned long long h = 0;
    int i,len;

    len = strlen(p);

    for (i=0;i<len;i++)
    {
        h += p[i];
        h += (h << 10);
        h ^= (h >> 6);
    }

    h += (h << 3);
    h ^= (h >> 11);
    h += (h << 15);

    return h % 30 + 1;
}

std::vector<std::string> getfiles(std::string directory){
	std::vector<std::string> v;
	DIR* dp;
	struct dirent* ep;
	dp=opendir(directory.c_str());

	if(dp!=NULL)
	{
		while((ep=readdir(dp))){
			// struct stat st;
			// lstat(ep->d_name, &st);

			// if(S_ISDIR(st.st_mode))
			// {
			// 	printf("%s is a directory\n",ep->d_name);
			// 	// std::std::vector<std::string> new_v = getfiles(directory+ep->d_name+"/");
			//  //   	v.insert (v.end(), new_v.begin(),new_v.end());
			// }
			// else
			if(!strcmp(ep->d_name,"..")==0 && !strcmp(ep->d_name,".")==0&&ep->d_name[0]!='.')
				v.push_back(ep->d_name );
		}
		closedir(dp);
	}
	else 
		perror("could not open Directory");
	return v;
}

class node
{
private:
	std::string node_addr;
	unsigned long long node_num;
	std::string succ;
	std::string predec;
public:
	node(std::string addr){
		node_addr = addr;
		node_num = consistent_hash(addr.c_str());
	}
	void set_s_p(std::string pred,std::string suc){
		succ = suc;
		predec = pred;
	}
	std::string get_node_addr(){
		return node_addr;
	}
	unsigned long long get_node_num(){
		return node_num;
	}
	node *successor(){
		return new node(succ);
	}
	node *predecessor(){
		return new node(predec);
	}
	int is_main_node()
	{
		std::string str(MAIN_NODE_IP);
		str += ":";
		str += MAIN_NODE_PORT;
		if(node_addr==str)
			return 1;
		return 0;
	}
};


class Client{	
public:
	struct sockaddr_in myaddr, serv_addr;
	int fd, i, slen;
	char buf[10000];	/* message buffer */
	int recvlen;
	char *server_name;
	int portno,type;
	Client(int socktype,int port,std::string servername) {
		/* # bytes in acknowledgement message */
		slen=sizeof(serv_addr);
		type=socktype;
		/* create a socket */

		if ((fd=socket(AF_INET, socktype, 0))==-1)
			printf("socket created\n");

		memset((char *)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		myaddr.sin_port = htons(0);

		memset((char *) &serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		if (inet_aton(servername.c_str(), &serv_addr.sin_addr)!=0) {
			inet_pton(AF_INET, servername.c_str(), &serv_addr.sin_addr);
		}
		server_name=strdup(servername.c_str());
		portno=port;	
	}
	int send_data(std::string data) {

		int res;
		if(type==SOCK_DGRAM){
			if ((res=sendto(fd, data.c_str(), data.size()+1,0, (struct sockaddr *)&serv_addr, slen))==-1) {
				perror("sendto");
			}
		}
		else{
			if((res=send(fd, data.c_str(), data.size()+1, 0))<0)
				perror("send_data:");
		}
		return res;
	}
	std::string receive_data() {
		int nbytes;
		socklen_t leng=sizeof(serv_addr);
		if(type==SOCK_DGRAM)
			nbytes = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&serv_addr, &leng);
		else
			nbytes=recv(fd, buf, sizeof(buf), 0);
		if (nbytes<0)
		{
			perror("receive_data:");
			sprintf(buf, "0");
		}
		return std::string(buf);
	}
	int connect_toserver() {
		int res=connect(fd, (sockaddr*)&serv_addr, slen);
		if(res<0)
			perror("connect_toserver:");
		printf("connected to server at %u\n",serv_addr.sin_addr.s_addr);
		return res;
	}	
};


class Server{	
public:
	node *current_node;
	std::string _successor,_predecessor;
	std::map<std::string, std::string> file_map;
	std::map<unsigned long long,std::string> node_map;
	int sockfd,newfd,newsockfd,type;
	sockaddr_in serv_addr,cli_addr;
	socklen_t clilen;
	std::map<std::string, in_addr_t> database;
	char buff[100];

	Server(int socket_type,int port){
		socklen_t clilen;
		char buffer[256];
		
		int n;

		sockfd = socket(AF_INET, socket_type, 0);
		if (sockfd < 0) 
			perror("ERROR opening socket");
		type=socket_type;

		bzero((char *) &serv_addr, sizeof(serv_addr));

		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(port);
		if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
			perror("ERROR on binding");
	}
	void set_node(std::string addr){
		current_node = new node(addr);
	}
	int start_listening(int waitno){
		listen(sockfd, waitno);
		clilen = sizeof(cli_addr);
     	return 0;
	}
	int accept_new(){
		newsockfd=accept(sockfd, (sockaddr*)&cli_addr, &clilen);
		return newsockfd;
	}
	int receive_data(sockaddr* addr,socklen_t* len){
		int nbytes=recvfrom(newsockfd, buff, sizeof(buff), 0,(sockaddr*)&cli_addr,len);
		if(nbytes<0)
			perror("ERROR in receive_data(int):");
		return nbytes;
	}
	int send_data(std::string data,sockaddr* addr,socklen_t &len){
		int res=sendto(newsockfd,data.c_str(), data.size()+1, 0, addr, len);
		if(res<0)
			perror("ERROR in send_data(int):");
		return res;
	}
	void change_successor(std::string pred){
		char _pred[100];
		strcpy(_pred,pred.c_str());
		char* token = strtok(_pred,":");
		char* ip=token;
		token = strtok(NULL,":");
		Client c(SOCK_DGRAM,atoi(token),ip);
		c.connect_toserver();
		std::string msg("CNGS ");
		msg += current_node->get_node_addr();
		c.send_data(msg);
	}
	void change_predecessor(std::string succ){
		char _succ[100];
		strcpy(_succ,succ.c_str());
		char* token = strtok(_succ,":");
		char* ip=token;
		token = strtok(NULL,":");
		Client c(SOCK_DGRAM,atoi(token),ip);
		c.connect_toserver();
		std::string msg("CNGP ");
		msg += current_node->get_node_addr();
		c.send_data(msg);
	}
	void take_files_from_successor(std::string succ){
		char _succ[100];
		strcpy(_succ,succ.c_str());
		char* token = strtok(_succ,":");
		char* ip=token;
		token = strtok(NULL,":");
		Client c(SOCK_DGRAM,atoi(token),ip);
		c.connect_toserver();
		std::string msg("DEL ");
		msg += current_node->get_node_addr();
		c.send_data(msg);

		std::string rcv_data = c.receive_data();

		char data[100];
		strcpy(data,rcv_data.c_str());
		char* token_1 = strtok(data,"@");
		token_1 = strtok(NULL,"@");

		while(token_1){
			std::string filename = token_1;
			token_1 = strtok(NULL,"@");
			std::string address = token_1;
			file_map.insert(pair<std::string,std::string>(filename,address));
			printf("--Stored %s(%lld) at %lld\n",filename.c_str(),consistent_hash(filename.c_str()),consistent_hash(address.c_str()));

			token_1 = strtok(NULL,"@");
		}
	}
	void ping_main_node(){
		Client c(SOCK_DGRAM,atoi(MAIN_NODE_PORT),MAIN_NODE_IP);
		c.connect_toserver();
		std::string msg("NEW ");
		msg += current_node->get_node_addr();
		c.send_data(msg);
		std::string rcv_data = c.receive_data();

		char data[100];
		strcpy(data,rcv_data.c_str());
		char* token = strtok(data+3," ");
		_predecessor=token;
		token = strtok(NULL," ");
		_successor=token;

		change_successor(_predecessor);
		change_predecessor(_successor);
		take_files_from_successor(_successor);
	}
	void forward(std::string rcv_data) {
		int send_count=0;
		int predecessor=consistent_hash(current_node->predecessor()->get_node_addr().c_str());
		int curr = current_node->get_node_num();
		std::string msg("FWD");
		char data[100];

		strcpy(data,rcv_data.c_str());
		char* token = strtok(data,"@");
		token = strtok(NULL,"@");

		while(token){
			std::string filename = token;
			token = strtok(NULL,"@");
			std::string address = token;

			int tmp_file_node = consistent_hash(filename.c_str());
			if(predecessor < curr) {
			 	if(tmp_file_node <= curr && tmp_file_node > predecessor){
					file_map.insert(pair<std::string,std::string>(filename,address));
					printf("--Stored %s(%lld) at %d\n",filename.c_str(),consistent_hash(filename.c_str()),curr);
				}
				else {
					printf("--Forwarded %s(%lld) to %s for storing\n",filename.c_str(),consistent_hash(filename.c_str()),current_node->successor()->get_node_addr().c_str());
					send_count++;
					msg += "@";
					msg += filename;	
					msg += "@";
					msg += address;
				}
			}
			else if(predecessor >= curr) {
				if(tmp_file_node > predecessor || tmp_file_node <= curr){
					file_map.insert(pair<std::string,std::string>(filename,address));
					printf("--Stored %s(%lld) at %d\n",filename.c_str(),consistent_hash(filename.c_str()),curr);
				}
				else {
					printf("--Forwarded %s(%lld) to %s for storing\n",filename.c_str(),consistent_hash(filename.c_str()),current_node->successor()->get_node_addr().c_str());
					send_count++;
					msg += "@";
					msg += filename;	
					msg += "@";
					msg += address;
				}
			}		

			token = strtok(NULL,"@");
		}
		if(send_count>0){
			char _succ[100];
			strcpy(_succ,current_node->successor()->get_node_addr().c_str());
			char* token = strtok(_succ,":");
			char* ip=token;
			token = strtok(NULL,":");
			Client c(SOCK_DGRAM,atoi(token),ip);
			c.connect_toserver();
			c.send_data(msg);
		}
	}
	void distribute_files(std::string dir){
		char directory[100];
		strcpy(directory,dir.c_str());
		std::string msg("FWD");
		std::vector<std::string> v=getfiles(directory);

		for(int i=0;i<v.size();i++)
		{
			msg += "@";
			msg += v[i];	
			msg += "@";
			msg += current_node->get_node_addr().c_str();
		}

		forward(msg);
	}
	std::string search(char* msg){
		char _succ[100];
		strcpy(_succ,current_node->successor()->get_node_addr().c_str());
		char* token = strtok(_succ,":");
		char* ip=token;
		token = strtok(NULL,":");
		printf("--File not found forwarding it to %s\n",current_node->successor()->get_node_addr().c_str());
		Client c(SOCK_DGRAM,atoi(token),ip);
		c.connect_toserver();
		c.send_data(msg);
		return c.receive_data();
	}
	void server_process(int port){
		while(1)
		{
			printf("--Server listenning\n");
			start_listening(2);
			accept_new();
			
			char buff[1000];
			int nbytes=recvfrom(sockfd, buff, sizeof(buff), 0,(sockaddr*)&cli_addr,&clilen);
			
			if(nbytes<0)
				perror("ERROR in server_process:");

			if(strncmp(buff,"NEW ",4)==0){
				std::string msg("PS ");
				char* ip_token = buff+4;
				int new_node = consistent_hash(ip_token);
				printf("--New node %d\n",new_node);
				int prev_size;
				prev_size=node_map.size();
				std::map<unsigned long long,std::string>::iterator it;
				std::map<unsigned long long,std::string>::iterator prev;
				prev = node_map.end();
				--prev;

				for (it=node_map.begin(); it!=node_map.end(); ++it) {
				    if(it->first > new_node){
				    	msg += prev->second;
				    	msg += " ";
				    	msg += it->second;

				    	node_map.insert(pair<unsigned long long,string>(new_node,ip_token));
				    	break;
				    }
				    prev = it;
				}
				if(prev_size==node_map.size()){
					std::map<unsigned long long,std::string>::iterator tmp = node_map.begin();
					msg += prev->second;
			    	msg += " ";
			    	msg += tmp->second;

			    	node_map.insert(pair<unsigned long long,string>(new_node,ip_token));
				}

				strcpy(buff,msg.c_str());
				int res=sendto(sockfd,buff, strlen(buff)+1, 0, (sockaddr*)&cli_addr, clilen);
				if(res<0)
					perror("ERROR in sending:");
			}
			else if(strncmp(buff,"CNGS ",5)==0){
				_successor = buff+5;
				printf("--Changed successor to %s\n",_successor.c_str());
				current_node->set_s_p(_predecessor,_successor);
			}
			else if(strncmp(buff,"CNGP ",5)==0){
				_predecessor = buff+5;
				printf("--Changed predecessor to %s\n",_predecessor.c_str());
				current_node->set_s_p(_predecessor,_successor);
			}
			else if(strncmp(buff,"DEL ",4)==0) {
				std::string msg("FILES");
				int predecessor = consistent_hash(buff+4);
				int curr = current_node->get_node_num();
				int tmp_file_node;
				printf("--Deleted these files and send to predecessor...............\n");
				for (std::map<std::string,std::string>::iterator it=file_map.begin(); it!=file_map.end(); ++it){
   					tmp_file_node = consistent_hash(it->first.c_str());
   					if(predecessor < curr) {
   					 	if(tmp_file_node <= predecessor || tmp_file_node > curr) {
	   						msg += "@";
	   						msg += it->first;
	   						msg += "@";
	   						msg += it->second;

	   						printf("%s\n",it->first.c_str());

	   						file_map.erase(it);
	   					}
   					}
   					else if(predecessor > curr) {
   						if(tmp_file_node <= predecessor && tmp_file_node > curr) {
	   						msg += "@";
	   						msg += it->first;
	   						msg += "@";
	   						msg += it->second;

	   						printf("%s\n",it->first.c_str());

	   						file_map.erase(it);
	   					}
   					}
				}
				printf("............................................................\n");
				strcpy(buff,msg.c_str());
				int res=sendto(sockfd,buff, strlen(buff)+1, 0, (sockaddr*)&cli_addr, clilen);
				if(res<0)
					perror("ERROR in sending:");
			}
			else if(strncmp(buff,"FWD",3)==0) {
				forward(std::string(buff));
			}
			else if(strncmp(buff,"SEARCH:",7)==0) {
				printf("--Files available.............................................\n");
				for (std::map<std::string,std::string>::iterator it=file_map.begin(); it!=file_map.end(); ++it)
				    std::cout << it->first << " => " << it->second << '\n';
				printf("..............................................................\n");

				std::string filename(buff+7);
				std::string msg;

				map<string,string>::const_iterator it = file_map.find(filename);
				if(it!=file_map.end())
					msg += it->second;
				else
					msg += search(buff);

				strcpy(buff,msg.c_str());
				int res=sendto(sockfd,buff, strlen(buff)+1, 0, (sockaddr*)&cli_addr, clilen);
				if(res<0)
					perror("ERROR in sending:");
			}
		}
	}
};

int main(){
	int port;
	printf("Enter port: ");
	scanf("%d",&port);

	key_t key = 100;
	int msgID = msgget(key,IPC_CREAT|0666);

	message buff;
	sprintf(buff.mtext,"%d",port);
	buff.mtype=1;
	msgsnd(msgID,&buff,strlen(buff.mtext),0);

	Server s(SOCK_DGRAM,port);

	std::string address("127.0.0.1");
	char tmp[5];
	sprintf(tmp,"%d",port);
	address += ":";
	address += tmp;

	s.set_node(address);

	if(!s.current_node->is_main_node()) {
		s.ping_main_node();
		s.current_node->set_s_p(s._predecessor,s._successor);
	}
	else {
		s.node_map.insert(pair<unsigned long long,string>(consistent_hash(address.c_str()),address));
		s.current_node->set_s_p(address,address);
	}

	printf("\nnode - %s (%lld)\n",s.current_node->get_node_addr().c_str(),s.current_node->get_node_num());
	printf("succ - %s\n",s.current_node->successor()->get_node_addr().c_str());
	printf("predec - %s\n\n",s.current_node->predecessor()->get_node_addr().c_str());

	std::string dir;
	printf("Enter directory: ");
	cin>>dir;
	s.distribute_files(dir);

	memset(&buff.mtext, 0, sizeof(buff.mtext));
	strcpy(buff.mtext,dir.c_str());
	buff.mtype=1;
	msgsnd(msgID,&buff,strlen(buff.mtext),0);

	s.server_process(port);

	return 0;
}