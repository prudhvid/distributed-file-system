
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <bits/stdc++.h>
#include "dirent.h"
#include <unistd.h>
#include "fcntl.h"
#include "sys/stat.h"

#define TRUE 1
#define FALSE 0
#define EQUAL(X,Y) (strcmp(X, Y)==0?1:0)

using std::string;

typedef struct{
	long mtype;
	char mtext[BUFSIZ];
}message;

void error(const char* msg){
	perror(msg);
	exit(1);
}

// std::vector<std::string> getfiles(std::string directory){
// 	std::vector<std::string> v;
// 	DIR* dp;
// 	struct dirent* ep;
// 	dp=opendir(directory.c_str());

// 	if(dp!=NULL)
// 	{
// 		while((ep=readdir(dp))){
// 			if(!EQUAL(ep->d_name,"..") && !EQUAL(ep->d_name,".")&&ep->d_name[0]!='.')
// 				v.push_back(ep->d_name );
// 		}
// 		closedir(dp);
// 	}
// 	else 
// 		error("could not open Directory");
// 	return v;
// }

class Client{	
public:
	struct sockaddr_in myaddr, serv_addr;
	int fd, i, slen;
	char buf[10000];	
	int recvlen;
	char *server_name;
	int portno,type;

	Client(int socktype,int port,std::string servername){
		slen=sizeof(serv_addr);
		type=socktype;

		if ((fd=socket(AF_INET, socktype, 0))==-1)
			printf("Socket created\n");

		memset((char *)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		myaddr.sin_port = htons(0);

		memset((char *) &serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		if (inet_aton(servername.c_str(), &serv_addr.sin_addr)!=0) {
			inet_pton(AF_INET, servername.c_str(), &serv_addr.sin_addr);
		}

		server_name=strdup(servername.c_str());
		portno=port;	
	}
	int send_data(std::string data){
		int res;
		if(type==SOCK_DGRAM){
			if ((res=sendto(fd, data.c_str(), data.size()+1, 
							0, (struct sockaddr *)&serv_addr, slen))==-1) {
				error("sendto");
			}
		}
		else{
			if((res=send(fd, data.c_str(), data.size()+1, 0))<0)
				error("send_data:");
		}
		return res;
	}
	std::string receive_data(){
		int nbytes;
		socklen_t leng=sizeof(serv_addr);
		if(type==SOCK_DGRAM)
			nbytes = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&serv_addr, &leng);
		else
			nbytes=recv(fd, buf, sizeof(buf), 0);
		if (nbytes<0)
		{
			error("receive_data:");
			sprintf(buf, "0");
		}
		return std::string(buf);
	}
	std::string read_data_ncopy(string name){
		int n_chars;
		
		int filesize=atoi((receive_data()).c_str());
		if (filesize!=0)
		{
			int wfd=creat(name.c_str(), S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
			printf("filesize=%d\n",filesize );
			int tsize=0,prevsize=0;
			while( (n_chars = read(fd, buf, sizeof(buf))) > 0 )
		  	{
		  		buf[n_chars+1]='\0';
		  		
				if( write(wfd, buf, n_chars) != n_chars )
				{
				  printf("error writing\n");
				}
				else{
					tsize+=n_chars;
					if((int)(tsize*100/filesize)!=prevsize)
					{
						printf("%f done\n ",tsize*1.0/filesize*100 );
						prevsize=tsize*100/filesize;
					}
				}
				if( n_chars == -1 )
				{
				  printf("error reading\n");
				}
		  	}
		  	close(wfd);
		  	printf("written successfully to %s\n",name.c_str());
		  }
		  else
		  	printf("--File not found or Server did not respond\n");

	  	return string(buf);
	}
	int connect_toserver(){
		int res=connect(fd, (sockaddr*)&serv_addr, slen);
		if(res<0)
			error("connect_toserver:");
		printf("connected to server at %u\n",serv_addr.sin_addr.s_addr);
		return res;
	}
};




class CopyServer{
public:
	int sockfd,newfd,newsockfd,type;
	sockaddr_in serv_addr,cli_addr;
	socklen_t clilen;
	std::map<std::string, in_addr_t> database;
	char buff[100];
	CopyServer(int socktype,int port)
	{
		
		socklen_t clilen;
		char buffer[256];
		int n;

		sockfd = socket(AF_INET, socktype, 0);
		if (sockfd < 0) 
			error("ERROR opening socket");
		type=socktype;

		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(port);
		if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
			error("ERROR on binding");
		
	}
	int start_listening(int waitno)
	{
		listen(sockfd, waitno);
		clilen = sizeof(cli_addr);
     	return 0;
	}
	int accept_new()
	{
		if(type==SOCK_DGRAM)
			return 0;
		newsockfd=accept(sockfd, (sockaddr*)&cli_addr, &clilen);
		return newsockfd;
	}
	/*for sock_stream*/
	int receive_data()
	{
		int res=recv(newsockfd, buff, sizeof(buff), 0);
		if(res<0)
			error("receive_data:");
		return res;
	}
	/*for datagram*/
	int receive_data(sockaddr* addr,socklen_t* len)
	{
		int nbytes=recvfrom(newsockfd, buff, sizeof(buff), 0,
					(sockaddr*)&cli_addr,len);
		if(nbytes<0)
			error("receive_data(int):");
		return nbytes;
	}
	/*for sock stream*/
	int send_data(std::string data)
	{
		int res=send(newsockfd, data.c_str(), data.size()+1, 0);
		if(res<0)
			error("send_data:");
		return res;
	}
	int send_data(int t)
	{
		char temp[50];
		sprintf(temp, "%d",t);
		return send_data(temp);
	}
	/*for datagram*/
	int send_data(std::string data,sockaddr* addr,socklen_t &len)
	{
		int res=sendto(newsockfd,data.c_str(), data.size()+1, 0, 
				addr, len);
			if(res<0)
				error("send_data(int):");
		return res;
	}
	void server_process(char* dir)
	{
		fd_set masterr,masterw,readfds,writefds;
		FD_ZERO(&masterr);
		FD_ZERO(&masterw);
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_SET(sockfd, &masterr);
		std::set<std::pair<int,int> > rwsets;
		int maxfd=sockfd;
		
		while(1)
		{

			timeval time_out;
			time_out.tv_sec=1;
			time_out.tv_usec=1000;
			readfds=masterr,writefds=masterw;
			if(select(maxfd+1, &readfds, &writefds, NULL,NULL )==-1)
				error("select:");

			if(FD_ISSET(sockfd, &readfds)){
				printf("FD_SET true\n");
				newsockfd=accept_new();
				receive_data();
				printf("new client asked for file %s!!\n",buff);
				std::string filename(buff);

				filename = dir + filename;
				printf("%s\n",filename.c_str());

				int readfd=open(filename.c_str(), O_RDONLY);
				if(readfd<0){
					perror("Error in opening file\n");
					close(newsockfd);
					continue;
				}
				struct stat st;
				stat(filename.c_str(),&st);
				send_data(st.st_size);
				printf("insert done\n");
				rwsets.insert(std::make_pair(newsockfd, readfd));
				FD_SET(newsockfd, &masterw);
				maxfd=std::max(newsockfd,maxfd);
			}
			
			std::set<std::pair<int,int> >::iterator it;
			std::vector<std::pair<int,int> > rmv;
			for(it=rwsets.begin();it!=rwsets.end();it++)
			{
				int nbytes=read(it->second, buff, sizeof(buff));
				if(nbytes==0)
				{
					close(it->second);
					printf("transferred file\n");
					close(it->first);
					
					std::pair<int,int> rmp=*it;
					
					FD_CLR(it->first, &masterw);
					rmv.push_back(rmp);
				}
				else{
					if(write(it->first, buff, nbytes)!=nbytes)
						printf("write error to client\n");
				}
			}
			for(int i=0;i<rmv.size();i++)
				rwsets.erase(rmv[i]);
		}
	}
};

int main(int argc, char const *argv[])
{
	std::string name="localhost";

	int port;
	key_t key = 100;
	int len=BUFSIZ,msgID = msgget(key,IPC_CREAT|0666);

	message buff;
	msgrcv(msgID,&buff,len,0,0);
	port = atoi(buff.mtext);

	Client c(SOCK_DGRAM,port,name);

	int serv_port = port+1;

	CopyServer s(SOCK_STREAM,serv_port);

	if(fork()==0)
	{
		message msg;
		msgrcv(msgID,&msg,len,0,0);

		s.start_listening(10);
		s.server_process(msg.mtext);
		exit(0);
	}
	sleep(2);

	while(1)
	{
		printf("\nEnter filename: ");
		char file[30];
		scanf("%s",file);

		printf("--Searching file %s\n",file);
		c.send_data(std::string("SEARCH:")+std::string(file) );
		printf("--File is present at  %s\n",address.c_str());
		std::string address=c.receive_data();

		if(address=="-1")
		{
			printf("file not found\n");
			continue;
		}

		char tmp[100];
		strcpy(tmp,address.c_str());
		char* token = strtok(tmp,":");
		std::string new_address(token);
		token = strtok(NULL,":");

		Client c2(SOCK_STREAM,atoi(token)+1,new_address.c_str());
		c2.connect_toserver();
		c2.send_data(file);
		c2.read_data_ncopy(file);
	}
	wait(0);
	return 0;
}