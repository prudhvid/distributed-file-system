#include <fstream>
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
#include "bits/stdc++.h"
#include "unistd.h"
#include "sys/types.h"
#include "dirent.h"
#include <stdlib.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "string.h"
#include "sys/stat.h"
#include "errno.h"
#include "dirent.h"
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>
#include "sys/wait.h"

using namespace ns3;
using namespace std;

void simulation(string name,u_int datarate)
{
    int ser_v[4]={4,5,2,3},cli_v[4]={0,1,6,7};
    Address serverAddress;
    FILE* fp=fopen((name+string("_data")).c_str(), "w");
    // freopen("net_ass3.tr", "w+", stdout);
    NodeContainer n;
    n.Create (8);
    RngSeedManager::SetSeed(time(NULL));
    RngSeedManager::SetRun(10);
    CsmaHelper csma;
    csma.SetChannelAttribute ("DataRate", DataRateValue (DataRate (1024*1024)));
    csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
    csma.SetDeviceAttribute ("Mtu", UintegerValue (1400));
    NetDeviceContainer d = csma.Install (n);

    InternetStackHelper internet;
    internet.Install (n);

    Ipv4AddressHelper ipv4;
    ipv4.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer i4 = ipv4.Assign (d);

    for(int i=0;i<4;i++)
    {
        serverAddress = Address(i4.GetAddress (ser_v[i]));

        uint16_t port = 9;
        UdpEchoServerHelper server (port);
        ApplicationContainer apps = server.Install (n.Get (ser_v[i]));
        apps.Start (Seconds (1.0));
        apps.Stop (Seconds (10.0));


        Ptr<UniformRandomVariable> x= CreateObject<UniformRandomVariable>();
        uint32_t maxPacketCount = 10000;
        Ptr<UniformRandomVariable> y= CreateObject<UniformRandomVariable>();
       
        uint packetsize=x->GetInteger(64,1024*40);
        Time interPacketInterval = Seconds (packetsize*1.0/datarate);

        UdpEchoClientHelper client (serverAddress, port);


        fprintf(fp, "%d %d %lf %d \n", cli_v[i],maxPacketCount,
        interPacketInterval.GetSeconds(),packetsize);
        //cleint number,maxPacketCount,interPacketInterval,packetsize
        client.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
        client.SetAttribute ("Interval", TimeValue (interPacketInterval));
        client.SetAttribute ("PacketSize", UintegerValue (packetsize));
        printf("%d %f\n",packetsize/1024,interPacketInterval.GetSeconds());
        apps = client.Install (n.Get (cli_v[i]));
        apps.Start (Seconds (2.0));
        apps.Stop (Seconds (10.0));
    }


    AsciiTraceHelper ascii;
    csma.EnableAsciiAll (ascii.CreateFileStream (name+".tr"));
    // csma.EnablePcapAll (name, false);

    Simulator::Run ();
    Simulator::Destroy ();
    fclose(fp);
}



int main(int argc, char const *argv[])
{
    mkdir("results", S_IRWXU);
    chdir("results");
    string numbers[]={"16","32","64","128","256","512","1024"};
    for (int numc = 0; numc <7 ; ++numc){
        string num(numbers[numc]);
        mkdir(num.c_str(), S_IRWXU);
        chdir(num.c_str());
        for(int i=0;i<10;i++)
        {
            char word[10];
            sprintf(word, "%d",i+1);
            string name=string(word);
            // mkdir(name.c_str(), S_IRWXU);
            // chdir(name.c_str());
            simulation(name,atoi(numbers[numc].c_str())*1024);
            // chdir("..");

        }
        chdir("..");
    }
    return 0;
}
