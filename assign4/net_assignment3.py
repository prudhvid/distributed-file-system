import os
import csv
import os.path
from math import sqrt
# import matlabplot.pyplot as plt

def mean(lst):
    return sum(lst)*1.0 / len(lst)

def variance(lst):
    mn = mean(lst)
    if len(lst)==1:
    	return 0
    variance = sum([(e-mn)**2 for e in lst])*1.0/(len(lst)-1)
    return variance

def stddev(lst):
	return sqrt(variance(lst))

server_client = {0:4,4:0,1:5,5:1,6:2,2:6,3:7,7:3}
server_nodes=[4,5,2,3]
path = "/home/prudhvi/workspace/networks/ass 3/tarballs/ns-allinone-3.21/ns-3.21"
size_dict = {16:"/results/16/",32:"/results/32/",64:"/results/64/",128:"/results/128/",256:"/results/256/",512:"/results/512/",1024:"/results/1024/"}

jitter_file=open(path+"/jitter.dat", "w+")
tp_file=open(path+"/tp.dat", "w+")
delay_file=open(path+"/delay.dat", "w+")

final_list_j=[]
final_list_tp=[]
final_list_d=[]

print jitter_file,tp_file,delay_file
for _i in size_dict:
	client_size_dict = {}
	avg_data = [0 for i in range(0,4)]
	jitter = []
	avg_delay=[]
	through_put =[]
	global_size = int(_i)
	avg_data[0] = global_size
	
	for index in range(1,11):
		
		inputfile = open(path+size_dict[_i]+str(index)+'.tr')
		inp_text = inputfile.readlines()

		f = open(path+size_dict[_i]+str(index)+'_data')
		param = f.readlines()

		for var in param:
			arr = var.split(' ')
			client_id = int(arr[0])
			size = int(arr[3])
			client_size_dict[client_id]=size

		only_udp_header = []
		delay_sum=0
		ids ={}

		for line in inp_text:
			if ("UdpHeader" in line):
				only_udp_header.append(line)

		for line in only_udp_header:
			words=line.split(' ')
			ids[words[18]]={}

		for i in ids:
			ids[i]={"+":[],"-":[],"d":[],"r":[]}
		f=open('abc.txt','w')
		for line in only_udp_header:
			f.writelines(line)
			words=line.split(' ')

			tmp=[]
			tmp.append(words[1])
			tmp.append(int(words[2][10]))

			ids[words[18]][words[0]].append(tmp)
		f.close()

		first_t_time = 100
		for i in ids:
			for t in ids[i]['-']:
				if first_t_time>float(t[0]):
					first_t_time=float(t[0])

		last_t_time = 0
		for i in ids:
			for t in ids[i]['-']:
				if last_t_time<float(t[0]):
					last_t_time=float(t[0])

		first_r_time = 100
		for i in ids:
			for r in ids[i]['r']:
				if first_r_time>float(r[0]):
					first_r_time=float(r[0])

		last_r_time = 0
		for i in ids:
			for r in ids[i]['r']:
				if last_r_time<float(r[0]):
					last_r_time=float(r[0]);

		time_diff = []
		for i in ids:
			for r in ids[i]['r']:
				r_mid = int(r[1])
				r_time = float(r[0])
				t_mid = server_client[r_mid]
				for t in ids[i]['+']:
					if int(t[1])==t_mid:
						diff = r_time - float(t[0])
						if diff <10: 
							time_diff.append(diff)

		avg_delay.append(mean(time_diff))
		jitter.append(variance(time_diff))

		tbytes = 0
		for i in ids:
			for t in ids[i]['-']:
				tmp_=t[1]
				if t[1] in server_nodes:
					tmp_=server_client[int(t[1])]
				tbytes = tbytes + client_size_dict[tmp_]

		through_put.append(tbytes/(float(last_t_time)-float(first_t_time)))


	# jitter_file.write(str(global_size)+'\t')
	# jitter_file.write(str(mean(jitter))+'\t')
	# jitter_file.write(str(stddev(jitter))+'\n')

	final_list_j.append((global_size,mean(jitter),stddev(jitter)))
	# print "mean jitter: ",mean(jitter),"standard deviation jitter: ",stddev(jitter) 
	# tp_file.write(str(global_size)+'\t')
	# tp_file.write(str(mean(through_put))+'\t')
	# tp_file.write(str(stddev(through_put))+'\n')

	final_list_tp.append((global_size,mean(through_put),stddev(through_put)))
	# print "mean through_put: ",mean(through_put),"standard deviation through_put: ",stddev(through_put) 
	# delay_file.write(str(global_size)+'\t')
	# delay_file.write(str(mean(avg_delay))+'\t')
	# delay_file.write(str(stddev(avg_delay))+'\n')


	final_list_d.append((global_size,mean(avg_delay),stddev(avg_delay)))
	# print "mean avg_delay: ",mean(avg_delay),"standard deviation avg_delay: ",stddev(avg_delay) 

final_list_d=sorted(final_list_d,key=lambda x:x[0])
final_list_j=sorted(final_list_j,key=lambda x:x[0])
final_list_tp=sorted(final_list_tp,key=lambda x:x[0])
print final_list_d,final_list_j,final_list_tp

for li in final_list_d:
	for li2 in li:
		delay_file.write(str(li2)+'\t')
	delay_file.write('\n')

for li in final_list_tp:
	for li2 in li:
		tp_file.write(str(li2)+'\t')
	tp_file.write('\n')

for li in final_list_j:
	for li2 in li:
		jitter_file.write(str(li2)+'\t')
	jitter_file.write('\n')

jitter_file.close()
delay_file.close()
tp_file.close()





