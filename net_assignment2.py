import os

inputfile = open('/home/sumit/ns-allinone-3.21/ns-3.21/udp-echo.tr')
outputfile = open('out.txt','w')

inp_text = inputfile.readlines()
only_udp_header = []
enqueue_list=[]
dequeue_list=[]
recieve_list=[]
dump_list=[]
delay_sum=0

for line in inp_text:
	if ("UdpHeader" in line):
		only_udp_header.append(line)

for line in only_udp_header:
	words=line.split(' ')
	tmp=[]
	tmp.append(words[0])
	tmp.append(words[1])
	tmp.append(words[38].replace('(size=','').replace(')',''))

	if words[0]=='+':
		enqueue_list.append(tmp)
	elif words[0]=='-':
		dequeue_list.append(tmp)
	elif words[0]=='r':
		recieve_list.append(tmp)
	elif words[0]=='d':
		dump_list.append(tmp)


outputfile.writelines("1. timeFirstTPacket: "+enqueue_list[0][1]+' sec\n')
outputfile.writelines("2. timeLastTPacket: "+enqueue_list[-1][1]+' sec\n')
outputfile.writelines("3. timeFirstRPacket: "+recieve_list[0][1]+' sec\n')
outputfile.writelines("4. timeLastRPacket: "+recieve_list[-1][1]+' sec\n')

for i in range(0,len(recieve_list)):
	delay_sum+=(float(recieve_list[i][1]) - float(enqueue_list[i][1]))

outputfile.writelines("5. delaySum: "+str(delay_sum)+' sec\n')
outputfile.writelines("6. tPackets: "+str(len(enqueue_list))+",\ttbytes: "+str(int(enqueue_list[0][2])*len(enqueue_list))+'\n')
outputfile.writelines("7. rPackets: "+str(len(recieve_list))+",\trbytes: "+str(int(recieve_list[0][2])*len(recieve_list))+'\n')
outputfile.writelines("8. lostPackets: "+str(len(enqueue_list)-len(recieve_list))+'\n')
