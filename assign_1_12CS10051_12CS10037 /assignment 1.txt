1.
my ip 203.110.243.23
neighbours 203.110.243.21 and 203.110.243.22
it can be same for us because load balancer is using some limited number of machines(ips).

2.
proxy server is a server that acts as an intermediary for requests from clients seeking resources from other servers. 
a. content control
b. improving performance 

3.
a. 
--- google.com ping statistics ---
packets transmit?ted 	4
received 	4
packet loss 	0 %
time 	3003 ms


--- Round Trip Time (rtt) ---
min 	37.046 ms
avg 	37.085 ms
max 	37.109 ms
mdev 	0.138 ms

b.

--- facebook.com ping statistics ---
packets transmitted 	4
received 	4
packet loss 	0 %
time 	3004 ms


--- Round Trip Time (rtt) ---
min 	107.662 ms
avg 	107.891 ms
max 	108.409 ms
mdev 	0.446 ms
 
c. 
--- 208.67.222.222 ping statistics ---
packets transmitted 	4
received 	4
packet loss 	0 %
time 	3003 ms


--- Round Trip Time (rtt) ---
min 	5.330 ms
avg 	5.345 ms
max 	5.357 ms
mdev 	0.090 ms

d. 
--- 10.3.100.207 ping statistics ---
packets transmitted 	9
received 	0
packet loss 	100 %
time 	8032 ms


4.
DNS is domain name system which basically converts your easy domain names like google.com to server address like 10.5.18.67

for ping.eu DNS server respons ping requests faster than google because from ping.eu (its in germany) traceroute for google.com is larger than open DNS server and wandwidth for google is lesser than DNS and respond time is masured bt both of them. But from some other place it can be larger than google.

5.  
a.
traceroute to google.com (195.122.16.49), 30 hops max, 60 byte packets
1 	  	  	  	*	*	*
2 	hos-tr3.juniper2.rz13.hetzner.de 	213.239.224.65 	de 	0.141 ms 	 	 
	hos-tr1.juniper1.rz13.hetzner.de 	213.239.224.1 	de 	0.293 ms 	 
	hos-tr2.juniper1.rz13.hetzner.de 	213.239.224.33 	de 	0.491 ms
3 	core22.hetzner.de 	213.239.245.121 	de 	0.672 ms 	 	 
	core21.hetzner.de 	213.239.245.81 	de 	0.871 ms 	 
	core22.hetzner.de 	213.239.245.121 	de 	0.672 ms
4 	core1.hetzner.de 	213.239.245.218 	de 	4.799 ms 	 	 
	core4.hetzner.de 	213.239.245.14 	de 	5.106 ms 	 
	core1.hetzner.de 	213.239.245.218 	de 	4.799 ms
5 	juniper1.ffm.hetzner.de 	213.239.245.5 	de 	6.129 ms 	4.890 ms 	4.899 ms
6 	riga-sa8-ic2-crs1.telekom.lv 	80.81.194.165 	de 	37.918 ms 	40.867 ms 	40.832 ms
7 	  	  	  	*	*	*
8 	  	  	  	*	*	*
9 	  	  	  	*	*	*
No reply for 3 hops. Assuming we reached firewall.

hops- 5
countries- Germany

b.
traceroute to facebook.com (173.252.120.6), 30 hops max, 60 byte packets
1 	  	  	  	*	*	*
2 	hos-tr3.juniper2.rz13.hetzner.de 	213.239.224.65 	de 	0.179 ms 	 	 
	hos-tr4.juniper2.rz13.hetzner.de 	213.239.224.97 	de 	0.274 ms 	 
	hos-tr3.juniper2.rz13.hetzner.de 	213.239.224.65 	de 	0.179 ms
3 	core21.hetzner.de 	213.239.245.81 	de 	0.276 ms 	0.286 ms 	 
	core22.hetzner.de 	213.239.245.121 	de 	0.270 ms
4 	core1.hetzner.de 	213.239.245.218 	de 	4.832 ms 	 	 
	core1.hetzner.de 	213.239.245.177 	de 	5.016 ms 	5.169 ms
5 	juniper1.ffm.hetzner.de 	213.239.245.5 	de 	5.197 ms 	5.173 ms 	5.142 ms
6 	ae1.br01.fra1.tfbnw.net 	80.81.194.40 	de 	5.191 ms 	4.965 ms 	5.130 ms
7 	be2.bb01.fra2.tfbnw.net 	31.13.27.203 	ie 	111.172 ms 	 	 
	be4.bb02.fra2.tfbnw.net 	31.13.24.18 	ie 	107.909 ms 	 
	be2.bb02.fra2.tfbnw.net 	31.13.27.205 	ie 	107.678 ms
8 	ae2.bb02.ams2.tfbnw.net 	74.119.78.92 	us 	34.343 ms 	 	 
	ae12.bb01.ams2.tfbnw.net 	74.119.78.40 	us 	24.729 ms 	 
	ae12.bb02.ams2.tfbnw.net 	74.119.79.14 	us 	21.181 ms
9 	ae8.bb01.lhr2.tfbnw.net 	31.13.30.195 	ie 	16.908 ms 	16.937 ms 	 
	ae8.bb02.lhr2.tfbnw.net 	74.119.76.65 	us 	16.904 ms
10 	be11.bb01.ewr2.tfbnw.net 	31.13.30.98 	ie 	106.597 ms 	108.189 ms 	109.392 ms
11 	be44.bb01.iad3.tfbnw.net 	31.13.26.1 	ie 	108.659 ms 	 	 
	be44.bb02.iad3.tfbnw.net 	31.13.26.5 	ie 	106.613 ms 	108.521 ms
12 	ae12.bb03.frc3.tfbnw.net 	31.13.24.88 	ie 	108.009 ms 	 	 
	be24.bb02.frc3.tfbnw.net 	173.252.64.206 	us 	111.902 ms 	 
	ae12.bb04.frc3.tfbnw.net 	31.13.24.95 	ie 	107.764 ms
13 	ae62.dr04.frc3.tfbnw.net 	173.252.65.107 	us 	109.112 ms 	 	 
	ae61.dr02.frc3.tfbnw.net 	173.252.64.63 	us 	109.306 ms 	109.305 ms
14 	  	  	  	*	*	*
15 	  	  	  	*	*	*
16 	edge-star-shv-12-frc3.facebook.com 	173.252.120.6 	us 	109.260 ms 	109.679 ms 	105.482 ms

hops- 13
countries- Germany, Ireland, Unites states

c.
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
1 	  	  	  	*	*	*
2 	hos-tr1.juniper1.rz13.hetzner.de 	213.239.224.1 	de 	0.292 ms 	0.444 ms 	 
	hos-tr3.juniper2.rz13.hetzner.de 	213.239.224.65 	de 	0.140 ms
3 	core21.hetzner.de 	213.239.245.81 	de 	0.542 ms 	 	 
	core22.hetzner.de 	213.239.245.121 	de 	0.521 ms 	 
	core21.hetzner.de 	213.239.245.81 	de 	0.542 ms
4 	core4.hetzner.de 	213.239.245.14 	de 	4.834 ms 	 	 
	core1.hetzner.de 	213.239.245.218 	de 	4.897 ms 	5.001 ms
5 	juniper4.ffm.hetzner.de 	213.239.245.1 	de 	4.985 ms 	 	 
	juniper1.ffm.hetzner.de 	213.239.245.5 	de 	4.987 ms 	4.984 ms
6 	  	  	  	*	*	*
7 	  	  	  	*	*	*
8 	  	  	  	*	*	*
No reply for 3 hops. Assuming we reached firewall.

hops- 4
countries- Germany

5.
firewall is a network security system that controls the incoming and outgoing network traffic based on an applied rule set.

uses--A firewall establishes a barrier between a trusted, secure internal network and another network. 

it fails because we can't access kgp server from outside kharagpur campus due to firewall.

6.
beacause we are doing it from kgp so it is a trusted internal network firewall wont reject it.

observation - time increases with the size of packet.

size 64b--
PING 10.3.100.207 (10.3.100.207) 56(84) bytes of data.
64 bytes from 10.3.100.207: icmp_seq=1 ttl=252 time=0.439 ms
64 bytes from 10.3.100.207: icmp_seq=2 ttl=252 time=0.411 ms
64 bytes from 10.3.100.207: icmp_seq=3 ttl=252 time=0.456 ms
64 bytes from 10.3.100.207: icmp_seq=4 ttl=252 time=0.446 ms
64 bytes from 10.3.100.207: icmp_seq=5 ttl=252 time=0.394 ms
64 bytes from 10.3.100.207: icmp_seq=6 ttl=252 time=0.396 ms
64 bytes from 10.3.100.207: icmp_seq=7 ttl=252 time=0.401 ms
64 bytes from 10.3.100.207: icmp_seq=8 ttl=252 time=0.428 ms
64 bytes from 10.3.100.207: icmp_seq=9 ttl=252 time=0.427 ms
64 bytes from 10.3.100.207: icmp_seq=10 ttl=252 time=0.413 ms
64 bytes from 10.3.100.207: icmp_seq=11 ttl=252 time=0.452 ms
64 bytes from 10.3.100.207: icmp_seq=12 ttl=252 time=0.412 ms
64 bytes from 10.3.100.207: icmp_seq=13 ttl=252 time=0.427 ms
64 bytes from 10.3.100.207: icmp_seq=14 ttl=252 time=0.458 ms
64 bytes from 10.3.100.207: icmp_seq=15 ttl=252 time=0.445 ms
64 bytes from 10.3.100.207: icmp_seq=16 ttl=252 time=0.477 ms
64 bytes from 10.3.100.207: icmp_seq=17 ttl=252 time=0.431 ms
64 bytes from 10.3.100.207: icmp_seq=18 ttl=252 time=0.439 ms
64 bytes from 10.3.100.207: icmp_seq=19 ttl=252 time=0.490 ms
64 bytes from 10.3.100.207: icmp_seq=20 ttl=252 time=0.387 ms

--- 10.3.100.207 ping statistics ---
20 packets transmitted, 20 received, 0% packet loss, time 19000ms
rtt min/avg/max/mdev = 0.387/0.431/0.490/0.033 ms

size 128b--
PING 10.3.100.207 (10.3.100.207) 120(148) bytes of data.
128 bytes from 10.3.100.207: icmp_seq=1 ttl=252 time=0.452 ms
128 bytes from 10.3.100.207: icmp_seq=2 ttl=252 time=0.499 ms
128 bytes from 10.3.100.207: icmp_seq=3 ttl=252 time=0.477 ms
128 bytes from 10.3.100.207: icmp_seq=4 ttl=252 time=0.400 ms
128 bytes from 10.3.100.207: icmp_seq=5 ttl=252 time=0.471 ms
128 bytes from 10.3.100.207: icmp_seq=6 ttl=252 time=0.475 ms
128 bytes from 10.3.100.207: icmp_seq=7 ttl=252 time=0.453 ms
128 bytes from 10.3.100.207: icmp_seq=8 ttl=252 time=0.488 ms
128 bytes from 10.3.100.207: icmp_seq=9 ttl=252 time=0.460 ms
128 bytes from 10.3.100.207: icmp_seq=10 ttl=252 time=0.449 ms
128 bytes from 10.3.100.207: icmp_seq=11 ttl=252 time=0.446 ms
128 bytes from 10.3.100.207: icmp_seq=12 ttl=252 time=0.444 ms
128 bytes from 10.3.100.207: icmp_seq=13 ttl=252 time=0.463 ms
128 bytes from 10.3.100.207: icmp_seq=14 ttl=252 time=0.439 ms
128 bytes from 10.3.100.207: icmp_seq=15 ttl=252 time=26.2 ms
128 bytes from 10.3.100.207: icmp_seq=16 ttl=252 time=0.430 ms
128 bytes from 10.3.100.207: icmp_seq=17 ttl=252 time=0.442 ms
128 bytes from 10.3.100.207: icmp_seq=18 ttl=252 time=0.444 ms
128 bytes from 10.3.100.207: icmp_seq=19 ttl=252 time=0.473 ms
128 bytes from 10.3.100.207: icmp_seq=20 ttl=252 time=0.370 ms

--- 10.3.100.207 ping statistics ---
20 packets transmitted, 20 received, 0% packet loss, time 19001ms
rtt min/avg/max/mdev = 0.370/1.739/26.223/5.617 ms


size 256b--
PING 10.3.100.207 (10.3.100.207) 248(276) bytes of data.
256 bytes from 10.3.100.207: icmp_seq=1 ttl=252 time=0.504 ms
256 bytes from 10.3.100.207: icmp_seq=2 ttl=252 time=0.466 ms
256 bytes from 10.3.100.207: icmp_seq=3 ttl=252 time=0.450 ms
256 bytes from 10.3.100.207: icmp_seq=4 ttl=252 time=0.443 ms
256 bytes from 10.3.100.207: icmp_seq=5 ttl=252 time=0.451 ms
256 bytes from 10.3.100.207: icmp_seq=6 ttl=252 time=0.535 ms
256 bytes from 10.3.100.207: icmp_seq=7 ttl=252 time=0.455 ms
256 bytes from 10.3.100.207: icmp_seq=8 ttl=252 time=0.491 ms
256 bytes from 10.3.100.207: icmp_seq=9 ttl=252 time=0.414 ms
256 bytes from 10.3.100.207: icmp_seq=10 ttl=252 time=0.471 ms
256 bytes from 10.3.100.207: icmp_seq=11 ttl=252 time=0.445 ms
256 bytes from 10.3.100.207: icmp_seq=12 ttl=252 time=0.473 ms
256 bytes from 10.3.100.207: icmp_seq=13 ttl=252 time=0.452 ms
256 bytes from 10.3.100.207: icmp_seq=14 ttl=252 time=0.458 ms
256 bytes from 10.3.100.207: icmp_seq=15 ttl=252 time=0.492 ms
256 bytes from 10.3.100.207: icmp_seq=16 ttl=252 time=0.519 ms
256 bytes from 10.3.100.207: icmp_seq=17 ttl=252 time=0.496 ms
256 bytes from 10.3.100.207: icmp_seq=18 ttl=252 time=0.465 ms
256 bytes from 10.3.100.207: icmp_seq=19 ttl=252 time=0.427 ms
256 bytes from 10.3.100.207: icmp_seq=20 ttl=252 time=0.504 ms

--- 10.3.100.207 ping statistics ---
20 packets transmitted, 20 received, 0% packet loss, time 19000ms
rtt min/avg/max/mdev = 0.414/0.470/0.535/0.037 ms


size 512b--
PING 10.3.100.207 (10.3.100.207) 504(532) bytes of data.
512 bytes from 10.3.100.207: icmp_seq=1 ttl=252 time=0.471 ms
512 bytes from 10.3.100.207: icmp_seq=2 ttl=252 time=0.454 ms
512 bytes from 10.3.100.207: icmp_seq=3 ttl=252 time=0.573 ms
512 bytes from 10.3.100.207: icmp_seq=4 ttl=252 time=0.491 ms
512 bytes from 10.3.100.207: icmp_seq=5 ttl=252 time=0.450 ms
512 bytes from 10.3.100.207: icmp_seq=6 ttl=252 time=0.451 ms
512 bytes from 10.3.100.207: icmp_seq=7 ttl=252 time=0.538 ms
512 bytes from 10.3.100.207: icmp_seq=8 ttl=252 time=0.527 ms
512 bytes from 10.3.100.207: icmp_seq=9 ttl=252 time=0.450 ms
512 bytes from 10.3.100.207: icmp_seq=10 ttl=252 time=0.501 ms
512 bytes from 10.3.100.207: icmp_seq=11 ttl=252 time=0.459 ms
512 bytes from 10.3.100.207: icmp_seq=12 ttl=252 time=0.463 ms
512 bytes from 10.3.100.207: icmp_seq=13 ttl=252 time=0.586 ms
512 bytes from 10.3.100.207: icmp_seq=14 ttl=252 time=0.464 ms
512 bytes from 10.3.100.207: icmp_seq=15 ttl=252 time=0.440 ms
512 bytes from 10.3.100.207: icmp_seq=16 ttl=252 time=0.445 ms
512 bytes from 10.3.100.207: icmp_seq=17 ttl=252 time=0.464 ms
512 bytes from 10.3.100.207: icmp_seq=18 ttl=252 time=0.455 ms
512 bytes from 10.3.100.207: icmp_seq=19 ttl=252 time=0.464 ms
512 bytes from 10.3.100.207: icmp_seq=20 ttl=252 time=0.493 ms

--- 10.3.100.207 ping statistics ---
20 packets transmitted, 20 received, 0% packet loss, time 19000ms
rtt min/avg/max/mdev = 0.440/0.481/0.586/0.051 ms

size 1024b--
1024 bytes from 10.3.100.207: icmp_seq=1 ttl=252 time=0.582 ms

1024 bytes from 10.3.100.207: icmp_seq=2 ttl=252 time=0.557 ms
1024 bytes from 10.3.100.207: icmp_seq=3 ttl=252 time=0.534 ms
1024 bytes from 10.3.100.207: icmp_seq=4 ttl=252 time=0.560 ms
1024 bytes from 10.3.100.207: icmp_seq=5 ttl=252 time=0.466 ms
1024 bytes from 10.3.100.207: icmp_seq=6 ttl=252 time=0.564 ms
1024 bytes from 10.3.100.207: icmp_seq=7 ttl=252 time=0.515 ms
1024 bytes from 10.3.100.207: icmp_seq=8 ttl=252 time=0.497 ms
1024 bytes from 10.3.100.207: icmp_seq=9 ttl=252 time=0.494 ms
1024 bytes from 10.3.100.207: icmp_seq=10 ttl=252 time=0.478 ms
1024 bytes from 10.3.100.207: icmp_seq=11 ttl=252 time=0.521 ms
1024 bytes from 10.3.100.207: icmp_seq=12 ttl=252 time=0.570 ms
1024 bytes from 10.3.100.207: icmp_seq=13 ttl=252 time=0.563 ms
1024 bytes from 10.3.100.207: icmp_seq=14 ttl=252 time=0.518 ms
1024 bytes from 10.3.100.207: icmp_seq=15 ttl=252 time=0.485 ms
1024 bytes from 10.3.100.207: icmp_seq=16 ttl=252 time=0.538 ms
1024 bytes from 10.3.100.207: icmp_seq=17 ttl=252 time=0.639 ms
1024 bytes from 10.3.100.207: icmp_seq=18 ttl=252 time=0.545 ms
1024 bytes from 10.3.100.207: icmp_seq=19 ttl=252 time=0.565 ms
1024 bytes from 10.3.100.207: icmp_seq=20 ttl=252 time=0.529 ms

--- 10.3.100.207 ping statistics ---
20 packets transmitted, 20 received, 0% packet loss, time 19001ms
rtt min/avg/max/mdev = 0.466/0.536/0.639/0.040 ms

size 2048b--
PING 10.3.100.207 (10.3.100.207) 2040(2068) bytes of data.
2048 bytes from 10.3.100.207: icmp_seq=1 ttl=252 time=0.648 ms
2048 bytes from 10.3.100.207: icmp_seq=2 ttl=252 time=0.625 ms
2048 bytes from 10.3.100.207: icmp_seq=3 ttl=252 time=0.631 ms
2048 bytes from 10.3.100.207: icmp_seq=4 ttl=252 time=0.616 ms
2048 bytes from 10.3.100.207: icmp_seq=5 ttl=252 time=0.642 ms
2048 bytes from 10.3.100.207: icmp_seq=6 ttl=252 time=0.671 ms
2048 bytes from 10.3.100.207: icmp_seq=7 ttl=252 time=0.621 ms
2048 bytes from 10.3.100.207: icmp_seq=8 ttl=252 time=0.627 ms
2048 bytes from 10.3.100.207: icmp_seq=9 ttl=252 time=0.609 ms
2048 bytes from 10.3.100.207: icmp_seq=10 ttl=252 time=0.608 ms
2048 bytes from 10.3.100.207: icmp_seq=11 ttl=252 time=0.604 ms
2048 bytes from 10.3.100.207: icmp_seq=12 ttl=252 time=0.645 ms
2048 bytes from 10.3.100.207: icmp_seq=13 ttl=252 time=0.677 ms
2048 bytes from 10.3.100.207: icmp_seq=14 ttl=252 time=0.636 ms
2048 bytes from 10.3.100.207: icmp_seq=15 ttl=252 time=1.03 ms
2048 bytes from 10.3.100.207: icmp_seq=16 ttl=252 time=0.679 ms
2048 bytes from 10.3.100.207: icmp_seq=17 ttl=252 time=0.677 ms
2048 bytes from 10.3.100.207: icmp_seq=18 ttl=252 time=0.577 ms
2048 bytes from 10.3.100.207: icmp_seq=19 ttl=252 time=0.651 ms
2048 bytes from 10.3.100.207: icmp_seq=20 ttl=252 time=0.603 ms

--- 10.3.100.207 ping statistics ---
20 packets transmitted, 20 received, 0% packet loss, time 19003ms
rtt min/avg/max/mdev = 0.577/0.654/1.037/0.093 ms

size 4096b--
PING 10.3.100.207 (10.3.100.207) 4088(4116) bytes of data.
4096 bytes from 10.3.100.207: icmp_seq=1 ttl=252 time=0.621 ms
4096 bytes from 10.3.100.207: icmp_seq=2 ttl=252 time=0.644 ms
4096 bytes from 10.3.100.207: icmp_seq=3 ttl=252 time=0.667 ms
4096 bytes from 10.3.100.207: icmp_seq=4 ttl=252 time=0.622 ms
4096 bytes from 10.3.100.207: icmp_seq=5 ttl=252 time=0.616 ms
4096 bytes from 10.3.100.207: icmp_seq=6 ttl=252 time=0.641 ms
4096 bytes from 10.3.100.207: icmp_seq=7 ttl=252 time=0.665 ms
4096 bytes from 10.3.100.207: icmp_seq=8 ttl=252 time=0.659 ms
4096 bytes from 10.3.100.207: icmp_seq=9 ttl=252 time=0.631 ms
4096 bytes from 10.3.100.207: icmp_seq=10 ttl=252 time=0.640 ms
4096 bytes from 10.3.100.207: icmp_seq=11 ttl=252 time=0.712 ms
4096 bytes from 10.3.100.207: icmp_seq=12 ttl=252 time=0.627 ms
4096 bytes from 10.3.100.207: icmp_seq=13 ttl=252 time=0.630 ms
4096 bytes from 10.3.100.207: icmp_seq=14 ttl=252 time=0.633 ms
4096 bytes from 10.3.100.207: icmp_seq=15 ttl=252 time=0.626 ms
4096 bytes from 10.3.100.207: icmp_seq=16 ttl=252 time=0.627 ms
4096 bytes from 10.3.100.207: icmp_seq=17 ttl=252 time=0.606 ms
4096 bytes from 10.3.100.207: icmp_seq=18 ttl=252 time=0.628 ms
4096 bytes from 10.3.100.207: icmp_seq=19 ttl=252 time=0.653 ms
4096 bytes from 10.3.100.207: icmp_seq=20 ttl=252 time=0.645 ms

--- 10.3.100.207 ping statistics ---
20 packets transmitted, 20 received, 0% packet loss, time 19000ms
rtt min/avg/max/mdev = 0.606/0.639/0.712/0.036 ms

7.
a. google
 195.122.16.39

b. facebook
 173.252.120.6


8.
port is application or process specific software.Is is end point of communication for hosts operating system.The purpose of ports is to uniquely identify different applications or processes running on a single computer and enable them to share a single physical connection to Internet.

SSH - 22
HTTP - 80
HTTPS - 443

9.
iitkgp.ac.in it serves only by HTTP that is port 80.

10.

HTTP
port 80
unsecured
operates at application layer
no encryption
no certificate requered

HTTPS
port 443
secured
operates at transport layer
encryption is present 
certificate is requered

11.Registrant Email -- head@cc.iitkgp.ernet.in

12. that is localhost which is our own computer. user can access the computer's own network services via its loopback network interface(get the output signals from your own machine).

localhost is useful for testing software during development independent of any networking configurations. If a computer has been configured to provide a website, directing its web browser to http://localhost may display its home page.



