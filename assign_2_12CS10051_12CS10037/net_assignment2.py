import os
import csv
import os.path

inputfile = open('/home/sumit/ns-allinone-3.21/ns-3.21/udp-echo.tr')
inp_text = inputfile.readlines()

f = open('parameters.txt')
param = f.readlines()
arr = param[0].replace('\n',"").split('/')
max_packet_size = int(arr[0])
time_interval = float(arr[1])
size = int(arr[2])

only_udp_header = []
delay_sum=0
ids ={}

for line in inp_text:
	if ("UdpHeader" in line):
		only_udp_header.append(line)

for line in only_udp_header:
	words=line.split(' ')
	ids[words[18]]={}

for i in ids:
	ids[i]={"+":[],"-":[],"d":[],"r":[]}

for line in only_udp_header:
	words=line.split(' ')

	tmp=[]
	tmp.append(words[1])
	tmp.append(int(words[2][10]))

	ids[words[18]][words[0]].append(tmp)

first_t_time = 100
for i in ids:
	for t in ids[i]['-']:
		if first_t_time>float(t[0]):
			first_t_time=float(t[0])

last_t_time = 0
for i in ids:
	for t in ids[i]['-']:
		if last_t_time<float(t[0]):
			last_t_time=float(t[0])

first_r_time = 100
for i in ids:
	for r in ids[i]['r']:
		if first_r_time>float(r[0]):
			first_r_time=float(r[0])

last_r_time = 0
for i in ids:
	for r in ids[i]['r']:
		if last_r_time<float(r[0]):
			last_r_time=float(r[0]);


for i in ids:
	if len(ids[i]['r'])==2 and len(ids[i]['+'])==2:
		delay_sum = delay_sum + (float(ids[i]['r'][0][0])-float(ids[i]['+'][0][0])) + (float(ids[i]['r'][1][0])-float(ids[i]['+'][1][0]))
	elif len(ids[i]['r'])==1 and len(ids[i]['+']==2):
		delay_sum = delay_sum + (float(ids[i]['r'][0][0])-float(ids[i]['+'][0][0]))

tPackets = 0
for i in ids:
	tPackets = tPackets + len(ids[i]['-'])
tbytes = tPackets * size

rPackets = 0
for i in ids:
	rPackets = rPackets + len(ids[i]['r'])
rbytes = rPackets * size

ttimesForwarded=0
for i in ids:
	zero_to_one=0
	for t in ids[i]['-']:
		if t[1]==0:
			zero_to_one = zero_to_one + 1
	ttimesForwarded = ttimesForwarded + zero_to_one-1
	ttimesForwarded = ttimesForwarded + len(ids[i]['-'])-zero_to_one-1

dPackets = 0
for i in ids:
	dPackets = dPackets + len(ids[i]['d'])
dbytes = dPackets * size

flag = False
if not os.path.isfile('temp.csv'):
	flag = True

fl = open('temp.csv', 'a')

writer = csv.writer(fl)
if flag==True:
	writer.writerow(['max_num_of_packets','time_b/w_two_packets','size(in bytes)', 'timeFirstTPacket', 'timeLastTPacket','timeFirstRPacket','timeLastRPacket','delaySum','tPackets/tBytes','rPackets/rBytes','lostPackets','totaltimesForwarded','packetsDropped/bytesDropped','transmitterThroughput','recieverThroughput'])
writer.writerow([max_packet_size,\
	time_interval,\
	size, \
 	first_t_time, \
 	last_t_time, \
 	first_r_time, \
 	last_r_time, \
 	delay_sum, \
 	str(tPackets)+"/"+str(tbytes), \
 	str(rPackets)+"/"+str(rbytes), \
 	tPackets- rPackets, \
 	ttimesForwarded, \
 	str(dPackets)+"/"+str(dbytes), \
	tbytes/(float(last_t_time)-float(first_t_time)), \
	rbytes/(float(last_r_time)-float(first_r_time))])

fl.close() 