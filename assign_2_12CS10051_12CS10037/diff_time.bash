rm data_diff_time.csv
cd /home/sumit/ns-allinone-3.21/ns-3.21/
./waf --run scratch/udp-echo --command-template "%s 1000 0.02 128"
cd /home/sumit/Desktop/net/
echo "1000/0.02/128" > parameters.txt
python net_assignment2.py
cd /home/sumit/ns-allinone-3.21/ns-3.21/
./waf --run scratch/udp-echo --command-template "%s 1000 0.05 128"
cd /home/sumit/Desktop/net/
echo "1000/0.05/128" > parameters.txt
python net_assignment2.py 
cd /home/sumit/ns-allinone-3.21/ns-3.21/
./waf --run scratch/udp-echo --command-template "%s 1000 0.1 128"
cd /home/sumit/Desktop/net/
echo "1000/0.1/128" > parameters.txt
python net_assignment2.py 
cd /home/sumit/ns-allinone-3.21/ns-3.21/
./waf --run scratch/udp-echo --command-template "%s 1000 1 128"
cd /home/sumit/Desktop/net/
echo "1000/1/128" > parameters.txt
python net_assignment2.py
rm parameters.txt 
mv temp.csv data_diff_time.csv