#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include "dirent.h"
#include "fcntl.h"
#include "sys/stat.h"

using std::string;
using std::pair;

typedef long long ll;

#define HASH_SPACE 32
#define INVALID_IP "NONE"
#define INVALID_PORT -1

#define MAIN_NODE_IP "127.0.0.1"
#define MAIN_NODE_PORT "8085"

#define NEW_JOIN "NEW_JOIN\n"
#define UPDATE_SP "UPDATE_SP\n"
#define UPDATE_DATA "UPDATE_DATA\n"
#define DONT_CARE "DONT_CARE\n"
#define SEARCH "SEARCH\n"
#define SEARCH_RESULT "SEARCH_RESULT\n"
#define KILL_YOURSELF "KILL_YOURSELF\n"


#define FINGER_HELP "FINGER_HELP\n"
#define FINGER_REPLY "FINGER_REPLY\n"
#define UPDATE_FINGER_TABLE "UPDATE_FINGER_TABLE\n"

#define DEL_NODE "DEL_NODE\n"
#define KILLALL "KILLALL\n"

#define GET_FILES "GET_FILES\n"

#define INF 1<<31

int mod (int a, int b)
{
   int ret = a % b;
   if(ret < 0)
     ret+=b;
   return ret;
}

void error(const char* msg)
{
	perror(msg);
	exit(1);
}


struct fulladdress
{
	string ip;
	int port;
	fulladdress(){
		this->ip=INVALID_IP;
		this->port=INVALID_PORT;
	}

	fulladdress(string ip,int port){
		this->ip=ip;
		this->port=port;
	}

	fulladdress(int ipv4,int port){
		sockaddr_in addr;
		addr.sin_family=AF_INET;
		addr.sin_addr.s_addr=ipv4;
		this->ip=inet_ntoa(addr.sin_addr);
		this->port=port;
	}
	
	string tostring(){
		char port_s[20];
		sprintf(port_s, "%d",port);
		return ip+":"+port_s;
	}
	sockaddr_in tosockaddr_in(){
		sockaddr_in ret;
		ret.sin_addr.s_addr=ip_int();
		ret.sin_port=htons(port);
		ret.sin_family=AF_INET;
		return ret;
	}
	static fulladdress fromstring(string s)
	{
		fulladdress r;
		int col=s.find(':');
		r.ip=s.substr(0,col);
		r.port=atoi(s.substr(col+1).c_str());
		return r;
	}
	int ip_int()
	{
		sockaddr_in addr;
		memset((char *) &addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		if (inet_aton(ip.c_str(), &addr.sin_addr)!=0) {
			inet_pton(AF_INET, ip.c_str(), &addr.sin_addr);
		}
		return addr.sin_addr.s_addr;
	}
	static sockaddr_in getsockaddr_in(string ip)
	{
		sockaddr_in addr;
		memset((char *) &addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		
		if (inet_aton(ip.c_str(), &addr.sin_addr)!=0) {
			inet_pton(AF_INET, ip.c_str(), &addr.sin_addr);
		}
		return addr;   
	}

};

fulladdress INVALID_ADDRESS(INVALID_IP,INVALID_PORT);


int operator==(fulladdress a,fulladdress b)
{
	if(a.ip==b.ip&&a.port==b.port)
		return 1;
	return 0;
}

typedef pair<int,fulladdress> maptype;
typedef pair<int,string> pis;
typedef pair<string,string> pss;


string operator+(string a,int i)
{
	char word[20];
	sprintf(word, "%d",i);
	return a+word;
}

string maptostring(maptype m)
{
	int fileid=m.first;
	string addr=m.second.tostring();
	char word[20];
	sprintf(word, "%d",fileid);
	string ret=word;
	ret+=" "+addr;
	return ret;
}

std::vector<std::string> split(std::string str,std::string sep){

	char *copy=strdup(str.c_str());
    char* cstr=const_cast<char*>(str.c_str());
    char* current;
    std::vector<std::string> arr;
    current=strtok(cstr,sep.c_str());
    while(current!=NULL){
        arr.push_back(current);
        current=strtok(NULL,sep.c_str());
    }
    strcpy(cstr, copy);
    return arr;
}

inline pss getheader(string s)
{
	char *copy=strdup(s.c_str());
	pss ret;
	char* cstr=const_cast<char*>(s.c_str());
    char* current;
	current=strtok(cstr,"\n");
	ret.first=current;
	ret.second=s.substr(ret.first.size()+1);
	strcpy(cstr, copy);

	return ret;
}

inline unsigned long long consistent_hash(const char *p){ 
	unsigned long long h = 0;
	int i,len;

	len = strlen(p);

	for (i=0;i<len;i++)
	{
		h += p[i];
		h += (h << 10);
		h ^= (h >> 6);
	}

	h += (h << 3);
	h ^= (h >> 11);
	h += (h << 15);

	return h % 30 + 1;
}
inline unsigned long long consistent_hash(string p)
{
	return consistent_hash(p.c_str());
}
inline std::vector<std::string> getfiles(std::string directory){
	std::vector<std::string> v;
	DIR* dp;
	struct dirent* ep;
	dp=opendir(directory.c_str());

	if(dp!=NULL)
	{
		while((ep=readdir(dp))){
			if(!strcmp(ep->d_name,"..")==0 && !strcmp(ep->d_name,".")==0&&ep->d_name[0]!='.')
				v.push_back(ep->d_name );
		}
		closedir(dp);
	}
	else 
		perror("could not open Directory");
	return v;
}



class Server{
	
public:
	int sockfd,newfd,newsockfd,type;
	sockaddr_in serv_addr,cli_addr;
	socklen_t clilen;
	std::map<std::string, in_addr_t> database;
	char buff[1000];
	Server(int socktype,int port)
	{
		
		socklen_t clilen;
		char buffer[256];
		
		int n;

		sockfd = socket(AF_INET, socktype, 0);
		if (sockfd < 0) 
			error("ERROR opening socket");
		type=socktype;

		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(port);
		if (bind(sockfd, (struct sockaddr *) &serv_addr,
								sizeof(serv_addr)) < 0) 
			error("ERROR on binding");
		
	}
	int getport()
	{
		return ntohs(serv_addr.sin_port);
	}
	int start_listening(int waitno)
	{
		listen(sockfd, waitno);
		clilen = sizeof(cli_addr);
		return 0;
	}
	int accept_new()
	{
		if(type==SOCK_DGRAM)
			return 0;
		newsockfd=accept(sockfd, (sockaddr*)&cli_addr, &clilen);
		if(newsockfd<0)
			error("accept:");
		return newsockfd;
	}
	/*for sock_stream*/
	int receive_data()
	{
		int res=recv(sockfd, buff, sizeof(buff), 0);
		if(res<0)
			error("receive_data:");
		return res;
	}
	/*for datagram*/
	string receive_data(sockaddr* addr,socklen_t* len)
	{
		char buf[1000];
		int nbytes=recvfrom(sockfd, buf, sizeof(buf), 0,
					(sockaddr*)&addr,len);
		if(nbytes<0)
			error("receive_data(int):");
		printf("PACKET_RECEIVED : %s\n",buf );
		sleep(2);
		return buf;
	}
	/*for sock stream*/
	int send_data(std::string data)
	{
		int res=send(sockfd, data.c_str(), data.size()+1, 0);
		if(res<0)
			error("send_data:");
		return res;
	}
	/*for datagram*/
	int send_data(std::string data,sockaddr* addr,socklen_t len=sizeof(sockaddr_in))
	{

		int res=sendto(sockfd,data.c_str(), data.size()+1, 0, 
				addr, len);
			if(res<0)
				error("send_data(int):");
		// sleep(2);
		return res;
	}
	void server_process()
	{
		char buff[1000];
		int nbytes=recvfrom(sockfd, buff, sizeof(buff), 0,
					(sockaddr*)&cli_addr,&clilen);
		if(nbytes<0)
			error("server_process:");
		
		
		char* file;
		int i=0;
		file=strtok(buff, "\n");
		if(atoi(file)==0){
			// printf("client %u  has files %s\n",cli_addr.sin_addr.s_addr,buff );  
			while(file!=NULL)
			{
				file=strtok(NULL, "\n");
				if(file){
					database[file]=cli_addr.sin_addr.s_addr;
					printf("added %s to database\n",file );
				}
			}
		}
		else
		{
			file=strtok(NULL, "\n");
			printf("searching for %s\n", file);

			
			if(database.find(file)!=database.end())
			{
				sprintf(buff, "%u",database[file]);
				printf("found file at %s\n", buff);
			}
				
			else
			{
				printf("not found\n");
				sprintf(buff, "-1");
			}
			int res=sendto(sockfd,buff, strlen(buff)+1, 0, 
				(sockaddr*)&cli_addr, clilen);
			
			
			if(res<0)
				error("sending:");
		}

	}
};






class CopyServer{
	
public:
	int sockfd,newfd,newsockfd,type;
	sockaddr_in serv_addr,cli_addr;
	socklen_t clilen;
	std::map<std::string, in_addr_t> database;
	char buff[1000];
	CopyServer(int socktype,int port)
	{
		
		socklen_t clilen;
		char buffer[256];
		
		int n;

		sockfd = socket(AF_INET, socktype, 0);
		if (sockfd < 0) 
			error("ERROR opening socket");
		type=socktype;

		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(port);
		if (bind(sockfd, (struct sockaddr *) &serv_addr,
								sizeof(serv_addr)) < 0) 
			error("ERROR on binding");
		
	}
	int getport()
	{
		return ntohs(serv_addr.sin_port);
	}
	int start_listening(int waitno)
	{
		listen(sockfd, waitno);
		clilen = sizeof(cli_addr);
     	return 0;
	}
	int accept_new()
	{
		if(type==SOCK_DGRAM)
			return 0;
		newsockfd=accept(sockfd, (sockaddr*)&cli_addr, &clilen);
		if(newsockfd<0)
			error("accept:");
		return newsockfd;
	}
	/*for sock_stream*/
	int receive_data()
	{
		int res=recv(newsockfd, buff, sizeof(buff), 0);
		if(res<0)
			error("receive_data:");
		return res;
	}
	/*for datagram*/
	int receive_data(sockaddr* addr,socklen_t* len)
	{
		int nbytes=recvfrom(newsockfd, buff, sizeof(buff), 0,
					(sockaddr*)&cli_addr,len);
		if(nbytes<0)
			error("receive_data(int):");
		return nbytes;
	}
	/*for sock stream*/
	int send_data(std::string data)
	{
		int res=send(newsockfd, data.c_str(), data.size()+1, 0);
		if(res<0)
			error("send_data:");
		return res;
	}
	int send_data(int t)
	{
		char temp[50];
		sprintf(temp, "%d",t);
		return send_data(temp);
	}
	/*for datagram*/
	int send_data(std::string data,sockaddr* addr,socklen_t &len)
	{
		int res=sendto(newsockfd,data.c_str(), data.size()+1, 0, 
				addr, len);
			if(res<0)
				error("send_data(int):");
		return res;
	}
	void server_process()
	{
		fd_set masterr,masterw,readfds,writefds;
		FD_ZERO(&masterr);
		FD_ZERO(&masterw);
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_SET(sockfd, &masterr);
		std::set<std::pair<int,int> > rwsets;
		int maxfd=sockfd;
		
		while(1)
		{

			timeval time_out;
			time_out.tv_sec=1;
			time_out.tv_usec=1000;
			readfds=masterr,writefds=masterw;
			if(select(maxfd+1, &readfds, &writefds, NULL,NULL )==-1)
				error("select:");

			if(FD_ISSET(sockfd, &readfds)){
				
				newsockfd=accept_new();
				receive_data();
				printf("new client asked for file %s!!\n",buff);
				std::string filename(buff);
				int readfd=open(filename.c_str(), O_RDONLY);
				if(readfd<0){
					perror("Error in opening file\n");
					close(newsockfd);
					continue;
				}
				struct stat st;
				stat(filename.c_str(),&st);
				send_data(st.st_size);
				
				rwsets.insert(std::make_pair(newsockfd, readfd));
				FD_SET(newsockfd, &masterw);
				maxfd=std::max(newsockfd,maxfd);
			}
			
			std::set<std::pair<int,int> >::iterator it;
			std::vector<std::pair<int,int> > rmv;
			for(it=rwsets.begin();it!=rwsets.end();it++)
			{
				int nbytes=read(it->second, buff, sizeof(buff));
				if(nbytes==0)
				{
					close(it->second);
					printf("transferred file\n");
					close(it->first);
					
					std::pair<int,int> rmp=*it;
					
					FD_CLR(it->first, &masterw);
					rmv.push_back(rmp);
				}
				else{
					if(write(it->first, buff, nbytes)!=nbytes)
						printf("write error to client\n");
				}
			}
			for(int i=0;i<rmv.size();i++)
				rwsets.erase(rmv[i]);
		}
	}
};


class Client{
	
public:
	struct sockaddr_in myaddr, serv_addr;
	int fd, i, slen;
	char buf[10000];	/* message buffer */
	int recvlen;
	char *server_name;
	int portno,type;
	Client(int socktype,int port,std::string servername)
	{
		/* # bytes in acknowledgement message */
		slen=sizeof(serv_addr);
		type=socktype;
		/* create a socket */

		if ((fd=socket(AF_INET, socktype, 0))==-1)
			printf("socket created\n");

		/* bind it to all local addresses and pick any port number */

		memset((char *)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		myaddr.sin_port = htons(0);

		if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
			perror("bind failed");
		}


		memset((char *) &serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		if (inet_aton(servername.c_str(), &serv_addr.sin_addr)!=0) {
			inet_pton(AF_INET, servername.c_str(), &serv_addr.sin_addr);
		}
		server_name=strdup(servername.c_str());
		portno=port;
		
	}
	Client(int socktype,int port,in_addr_t addr)
	{
		slen=sizeof(serv_addr);
		type=socktype;
		/* create a socket */

		if ((fd=socket(AF_INET, socktype, 0))==-1)
			printf("socket created\n");

		/* bind it to all local addresses and pick any port number */

		memset((char *)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		myaddr.sin_port = htons(0);

		if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
			perror("bind failed");
		}


		memset((char *) &serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		serv_addr.sin_addr.s_addr=addr;
		portno=port;
	}
	int send_data(std::string data)
	{

		// printf("Sending packet %d to %s port %d\n", i, server_name, portno);
		
		int res;
		if(type==SOCK_DGRAM){
			if ((res=sendto(fd, data.c_str(), data.size()+1, 
							0, (struct sockaddr *)&serv_addr, slen))==-1) {
				error("sendto");
			}
		}
		else{
			if((res=send(fd, data.c_str(), data.size()+1, 0))<0)
				error("send_data:");
		}
		return res;
	}
	std::string receive_data()
	{
		int nbytes;
		socklen_t leng=sizeof(serv_addr);
		if(type==SOCK_DGRAM)
			nbytes = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&serv_addr, &leng);
		else
			nbytes=recv(fd, buf, sizeof(buf), 0);
		if (nbytes<0)
		{
			error("receive_data:");
			sprintf(buf, "0");
		}
		return std::string(buf);
	}
	std::string read_data_ncopy(string name)
	{
		int n_chars;
		int wfd=creat(name.c_str(), S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
		int filesize=atoi((receive_data()).c_str());
		printf("filesize=%d\n",filesize );
		int tsize=0,prevsize=0;
		while( (n_chars = read(fd, buf, sizeof(buf))) > 0 )
	  	{
	  		buf[n_chars+1]='\0';
	  		
			if( write(wfd, buf, n_chars) != n_chars )
			{
			  printf("error writing\n");
			}
			else{
				tsize+=n_chars;
				if((int)(tsize*100/filesize)!=prevsize)
				{
					printf("%f done\n ",tsize*1.0/filesize*100 );
					prevsize=tsize*100/filesize;
				}
					

			}
				
		 
		 
			if( n_chars == -1 )
			{
			  printf("error reading\n");
			}
	  	}
	  	close(wfd);
	  	printf("written successfully to %s\n",name.c_str());
	  	return string(buf);
	}
	int connect_toserver()
	{
		int res=connect(fd, (sockaddr*)&serv_addr, slen);
		if(res<0)
			error("connect_toserver:");
		printf("connected to server at %u\n",serv_addr.sin_addr.s_addr);
		return res;
	}
	
};

