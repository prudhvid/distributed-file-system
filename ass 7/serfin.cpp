#include "common.h"

typedef std::vector<string> strings;
using namespace std;

class node
{
private:
	fulladdress node_addr;
	unsigned long long node_num;
	fulladdress succ;
	fulladdress predec;
	Server ser;
	CopyServer cser;
	string filename;
	fulladdress fingertable[5];
	string datanames[HASH_SPACE];
public:
	int cpid;
	std::map<int, fulladdress> data;

	node(std::string addr)
		:ser(SOCK_DGRAM,fulladdress::fromstring(addr).port),
			cser(SOCK_STREAM,10000+rand()%10000)
	{
		
		node_num = consistent_hash(addr.c_str());
		node_addr=fulladdress::fromstring(addr);
		printf("server at %s\n",  fulladdress(ser.serv_addr.sin_addr.s_addr,ser.getport()).tostring().c_str() );
		cser.start_listening(10);
		if((cpid=fork())==0)
		{
			printf("CopyServer started on port no %d\n",cser.getport() );
			cser.server_process();
			exit(0);
		}
	}

	void print_info()
	{
		printf("ID=%llu   predecessor=%llu   succ= %llu\n", node_num,consistent_hash(predec.tostring()),
						consistent_hash(succ.tostring()));

		printf("file index \n");
		map<int,fulladdress>::iterator it=data.begin();
		for(;it!=data.end();it++)
			printf("%s %d\n",datanames[it->first].c_str(),it->first );

		printf("FINGER_TABLE\n");
		for (int i = 0; i < 5; ++i){
			printf("%d (%llu) --> %llu\n",i,((1<<i)+node_num)%HASH_SPACE, 
							consistent_hash(fingertable[i].tostring()));
		}
		string packet=GET_FILES;
		packet+=node_addr.tostring()+"\n";
		send_to_succ(packet);
	}

	void init(string dir=".")
	{
		this->succ=this->predec=this->node_addr;

		if(this->node_addr.tostring()!=string(MAIN_NODE_IP)+":"+MAIN_NODE_PORT)
			while(1){
				send_to_root(NEW_JOIN+node_addr.tostring());
				sockaddr_in si;
				socklen_t l=sizeof(si);

				string update_packet=ser.receive_data((sockaddr*)&si, &l);
				
				std::vector<string> v=split(update_packet, "\n");
				if(v[0]+"\n"!=UPDATE_SP)
				{
					if(v[0]+"\n"==KILL_YOURSELF)
					{
						printf("Sorry node name already exists\n");
						kill(cpid, SIGABRT);
						exit(0);
					}
				}
				else{
					set_s_p(fulladdress::fromstring(v[2]), fulladdress::fromstring(v[1]));
					
					// printf("id %llu pred %llu succ %llu\n",node_num,consistent_hash(predec.tostring().c_str())
					// 	,consistent_hash(succ.tostring().c_str()));

					std::vector<string> v=getfiles(dir);

					string forward=UPDATE_DATA;
					for (int i = 0; i < v.size(); ++i){

						maptype m;
						m.first=consistent_hash(v[i].c_str());
						m.second= fulladdress(cser.serv_addr.sin_addr.s_addr,cser.getport());
						forward+="+ "+maptostring(m)+" "+v[i] +"\n";
					}
					send_to_succ(forward);

					sleep(2);
					build_fingertable();
					sleep(2);
					string update_finger=UPDATE_FINGER_TABLE;
					update_finger+=node_addr.tostring();
					send_to_succ(update_finger);

					break;
				}
			}
		else{
			printf("Root node started on %s\n",node_addr.tostring().c_str() );
			for (int i = 0; i < 5; ++i){
				fingertable[i]=node_addr;
			}

		}
			
		while(1)
		{
			printf("Enter filename\n");
			fd_set masterr;
			fd_set errorr;
			FD_ZERO(&masterr);
			FD_ZERO(&errorr);

			FD_SET(ser.sockfd, &masterr);
			FD_SET(STDIN_FILENO, &masterr);
			FD_SET(SIGINT, &errorr);

			if(select(ser.sockfd+1, &masterr, NULL, &errorr, NULL)<0)
				error("select:");

			if(FD_ISSET(SIGINT, &errorr))
			{
				printf("KILLALL!\n");
				send_to_succ(KILLALL);
				kill(cpid, SIGABRT);
				exit(0);
			}

			if(FD_ISSET(STDIN_FILENO, &masterr)){
				char word[100];
				scanf(" %s",word);
				
				filename=word;
				if(filename=="ls")
				{
					print_info();
					continue;
				}

				printf("searching for %s\n",word );
				int id=consistent_hash(word);
				
				if(data.find(id)!=data.end()){
					searchresult(data[id].tostring());
				}
				else{
					search_query(word);
				}
			}

			if(FD_ISSET(ser.sockfd, &masterr)){
				socklen_t len;
				sockaddr_in ad;
				string packet=ser.receive_data((sockaddr*)&ad, &len);
				
				pss p=getheader(packet);
				
				string header=p.first,tail=p.second;
				// printf("header=%s,tail=%s\n",header.c_str(),tail.c_str() );
				header+="\n";
				if(header==SEARCH)
					this->search(tail);
				if(header==NEW_JOIN)
					this->new_join(tail);
				if(header==UPDATE_DATA)
					this->update_data(tail);
				if(header==UPDATE_SP)
					updatesp(tail);
				if(header==SEARCH_RESULT)
					searchresult(tail);
				if(header==KILL_YOURSELF){
					printf("Sorry node name already exists\n");
					kill(cpid, SIGABRT);
					exit(0);
				}
				if(header==FINGER_HELP)
					finger_help(tail);
				if(header==FINGER_REPLY)
					finger_reply(tail);
				if(header==UPDATE_FINGER_TABLE)
					update_finger_table(tail);
				if(header==KILLALL){
					printf("root node gone.. exiting\n");
					send_to_succ(KILLALL);
					kill(cpid, SIGABRT);
					exit(0);
				}
				if(header==DEL_NODE)
					del_node(tail);
				if(header==GET_FILES)
					get_files(tail);
			}

			
		}

	}

	void set_s_p(fulladdress pred,fulladdress suc){
		succ = suc;
		predec = pred;
	}
	fulladdress get_node_addr(){
		return node_addr;
	}
	unsigned long long get_node_num(){
		return node_num;
	}
	fulladdress successor(){
		return succ;
	}
	fulladdress predecessor(){
		return (predec);
	}
	int is_main_node()
	{
		std::string str(MAIN_NODE_IP);
		str += ":";
		str += MAIN_NODE_PORT;
		if(str==node_addr.tostring())
			return 1;
		return 0;
	}
	void add_file(int fileid,fulladdress f,string name)
	{
		printf("added %d to index\n", fileid);
		data[fileid]=f;
		
		datanames[fileid]=name;
	}
	void remove_file(int fileid)
	{
		printf("removed file %d\n",fileid );
		data.erase(fileid);
	}
	
	//init inclusive
	std::vector<pair<int,fulladdress> > getfilesbetween(int init,int final)
	{
		std::vector<pair<int, fulladdress> > v;

		std::map<int, fulladdress>::iterator it=data.begin();

		for(;it!=data.end();it++){
			// if(it->first>=init && it->first<final)
			v.push_back(*it);
		}
		return v;
	}
	void send_to_succ(string s)
	{
		// printf("forwarding to successor %llu\n",consistent_hash(succ.tostring().c_str() ));
		sockaddr_in sadd=succ.tosockaddr_in();
		socklen_t len=sizeof(sadd);
		// sleep(2);
		ser.send_data(s, (sockaddr*)&sadd, len);
		
	}
	void send_to_pred(string s)
	{
		// printf("forwarding to predecessor %llu\n",consistent_hash(predec.tostring().c_str() ));
		sockaddr_in sadd=predec.tosockaddr_in();
		socklen_t len=sizeof(sadd);
		// sleep(2);
		ser.send_data(s, (sockaddr*)&sadd, len);
		
	}
	void send_to_root(string s)
	{
		fulladdress root(MAIN_NODE_IP,atoi(MAIN_NODE_PORT));
		sockaddr_in sadd=root.tosockaddr_in();
		socklen_t len=sizeof(sadd);
		// sleep(2);
		printf("sent data to root %s %d\n",s.c_str(),ser.send_data(s, (sockaddr*)&sadd, len));
		
	}

	void new_join(string addr)
	{
		
		int id=consistent_hash(addr.c_str());
		printf("new_join packet arrived with id=%d\n",id);

		int pred_id=consistent_hash(predec.tostring().c_str());
		int succ_id=consistent_hash(succ.tostring().c_str());
		
		if(id==node_num){
			printf("Dropping packet with same id\n");
			string packet=KILL_YOURSELF;
			socklen_t l=sizeof(sockaddr_in);
			sockaddr_in s= fulladdress::fromstring(addr).tosockaddr_in();

			ser.send_data(packet,  (sockaddr*)&s, l);
			return;
		}

		if(pred_id==node_num&&succ_id==node_num&&
			node_addr.tostring()==MAIN_NODE_IP+string(":")+ MAIN_NODE_PORT)
		{
			//this is root
			printf("I'm root\n");
			this->predec=fulladdress::fromstring(addr);
			this->succ=fulladdress::fromstring(addr);
			string update=UPDATE_SP;
			update+=node_addr.tostring()+"\n";
			update+=node_addr.tostring();

			send_to_succ(update);
		}

		else if( (node_num>pred_id && id<node_num && id>pred_id) ||
			(node_num<pred_id && (id>pred_id ||  id< node_num  ) ))
		{
			//valid id for becoming its predecessor
			
			/*
			send to actual predecessor about updating predessor
			*/
			string update=UPDATE_SP;
			update+=addr+"\n";
			update+=DONT_CARE;
			send_to_pred(update);
			
			/*
			send to updated predecessor about its both 
			successor and predecessor
			*/
			string prev_pred=predec.tostring();
			this->predec=fulladdress::fromstring(addr);

			update=UPDATE_SP;
			update+=node_addr.tostring()+"\n";
			update+=prev_pred;
			send_to_pred(update);


			/*
				send fids to the new predecessor
			*/
			std::vector<maptype> v=getfilesbetween(-INF,INF );
			string change_data=UPDATE_DATA;
			for (int i = 0; i < v.size(); ++i){
				if(!issuccessorof(v[i].first)){
					change_data+="+ "+ maptostring(v[i])+" "+ datanames[v[i].first] +"\n";
					remove_file(v[i].first);
				}
			}
			
			if(change_data!=UPDATE_DATA)
			{
				if(change_data[change_data.size()-1]=='\n')
					change_data=change_data.substr(0,change_data.size()-1);
				send_to_pred(change_data);
			}
				
		}
		else{
			string s;
			s=NEW_JOIN+addr;
			send_to_succ(s);
		}

		// update_finger_table(addr);
	}


	int issuccessorof(int id)
	{
		int succ_id=consistent_hash(succ.tostring().c_str());
		int pred_id=consistent_hash(predec.tostring().c_str());
		if(node_num>pred_id){
			if(id<=node_num && id>pred_id)
				return 1;
		}
		else{
			if(id>pred_id)
				return 1;
			else if(id<=node_num)
				return 1;
		}
		return 0;

	}
	int issuccessorof(int id,int pred_id,int succ_id)
	{
		if(node_num>pred_id){
			if(id<=node_num && id>pred_id)
				return 1;
		}
		else{
			if(id>pred_id)
				return 1;
			else if(id<=node_num)
				return 1;
		}
		return 0;
	}
	

	string update_data(string s)
	{
		printf("update_data packet arrived with %s\n",s.c_str());
		int succ_id=consistent_hash(succ.tostring().c_str());
		int pred_id=consistent_hash(predec.tostring().c_str());
		string forward=UPDATE_DATA;


		std::vector<string> v=split(s, "\n");

		
		for (int i = 0; i < v.size(); ++i){
			
			std::vector<string> v2=split(v[i], " ");
			
			int id=atoi(v2[1].c_str());
			if(v2[0]=="+"){

				fulladdress ad=fulladdress::fromstring(v2[2]);	
				if(node_num>pred_id){
					if(id<=node_num && id>pred_id)
						add_file(id, ad,v2[3]);
					else
						forward+=v[i]+"\n";
				}
				else{
					if(id>pred_id)
						add_file(id,ad,v2[3] );
					else if(id<=node_num)
						add_file(id, ad,v2[3]);
					else
						forward+=v[i]+"\n";
				}
			}

			else{
				if(data.find(id)!=data.end())
					remove_file(id);
				else
					forward+=v[i]+"\n";
			}
		
		}
		if(forward!=UPDATE_DATA)
			send_to_succ(forward);
		return forward;
	}

	void search(string s)
	{
		printf("search packet arrived with %s\n",s.c_str());
		std::vector<string> v=split(s, " ");
		int id=atoi(v[0].c_str());

		int final_id=atoi(v[1].c_str());

		

		sockaddr_in dest=fulladdress::fromstring(v[2]).tosockaddr_in();

		
		
		



		if(data.find(id)!=data.end()){
			string packet=SEARCH_RESULT;
			packet+=data[id].tostring();

			sockaddr_in addr=dest;
			socklen_t len=sizeof(addr);
			ser.send_data(packet, (sockaddr*)&addr,len);
		}
		else if(final_id==node_num){
			printf("Ended search space .. Dropping packet\n");
			string packet=SEARCH_RESULT;
			packet+=INVALID_ADDRESS.tostring();
			ser.send_data(packet, (sockaddr*)&dest);
			return;
		}

		else
			send_to_pred(SEARCH+ s);


	}
	void searchresult(string s)
	{
		printf("searchresult packet arrived with %s\n",s.c_str());
		fulladdress f=fulladdress::fromstring(s);
		if(f.ip==INVALID_ADDRESS.ip)
		{
			printf("FIle Not found!\n");
			return;
		}

		Client c(SOCK_STREAM,f.port,f.ip.c_str());
		c.connect_toserver();
		c.send_data(filename);
		c.read_data_ncopy(filename);
		// exit(0);
		
		return;
	}
	
	void search_query(string file)
	{
		int id=consistent_hash(file);
		printf("id=%d\n", id);

		for (int i = 0; i < 5; ++i){
			// int val=((1<<i)+node_num)%HASH_SPACE;
			int pr=(i==0)?(node_num+(1<<4)+1)%HASH_SPACE:((1<<(i-1))+node_num)%HASH_SPACE;
			int su=((1<<i)+node_num)%HASH_SPACE;

			if((su>pr && id>pr && id<=su ) || (su<pr && (id>pr || id<=su)))
			{
				string packet=SEARCH;
				packet=packet+id+" ";
				packet=packet+consistent_hash(fingertable[mod(i-1, 5)].tostring())+" ";
				packet+=node_addr.tostring();

				sockaddr_in addr= fingertable[i].tosockaddr_in();

				ser.send_data(packet,(sockaddr*)&addr );
				printf("sent search request to id %llu\n", consistent_hash(fingertable[i].tostring()));
			}
		}
		printf("search ended\n");
	}
	void updatesp(string s)
	{
		
		std::vector<string> v=split(s, "\n");

		if(v[0]+"\n"!=DONT_CARE){
			this->succ=fulladdress::fromstring(v[0]);
			printf("updated successor as %llu\n",consistent_hash(v[0].c_str()) );
		}
		if(v[1]+"\n"!=DONT_CARE){
			this->predec=fulladdress::fromstring(v[1]);
			printf("updated predecessor as %llu\n",consistent_hash(v[1].c_str()) );
		}
	}

	
	void finger_help(string s)
	{

		strings ss=split(s," ");
		printf("finger_help from %llu\n",consistent_hash(ss[1].c_str()) );
		int n=atoi(ss[0].c_str());

		if(issuccessorof(n%HASH_SPACE))
		{
			string packet=FINGER_REPLY;
			packet=packet+n+" ";
			packet+=node_addr.tostring();

			sockaddr_in in=fulladdress::fromstring(ss[1]).tosockaddr_in();
			ser.send_data(packet, (sockaddr*)&in  );
			printf("%llu is the right successor of %d\n",node_num,n );
		}
		else
			send_to_succ(FINGER_HELP+s);
	}
	
	void finger_reply(string s)
	{
		strings v=split(s, " ");
		int n=atoi(v[0].c_str());
		n-=node_num;
		int c=0;
		while(n>1)
			n=n>>1,c++;
		fingertable[c]= fulladdress::fromstring(v[1]);
		printf("updated fingertable entry as f[%d]=%llu\n", c,consistent_hash(v[1]));
	}

	void build_fingertable()
	{
		fingertable[0]=succ;
		for (int i = 1; i < 5; ++i)
		{
			int val=(1<<i)+node_num;
			// printf("node_num=%llu and val=%d and added =%d\n",node_num,val,1<<i );
			string packet=FINGER_HELP;
			packet=packet+val+" ";
			packet+=node_addr.tostring();
			send_to_succ(packet);
			
		}
	}

	void update_finger_table(string s)
	{
		int id=consistent_hash(s.c_str());
		fulladdress addr=fulladdress::fromstring(s);
		if(id==node_num)
			return;

		for (int i = 0; i < 5; ++i){
			
			int val=(node_num+(1<<i))%HASH_SPACE;
			int fval=consistent_hash(fingertable[i].tostring());

			if(val<=fval){
				if(id>=val&&id<fval)
					fingertable[i]=addr;

			}
			else{
				if(id>=val)
					fingertable[i]=addr;
				else if(id<fval)
					fingertable[i]=addr;
			}
		}
		send_to_succ(UPDATE_FINGER_TABLE+s);
	}


	void delete_self()
	{
		printf("Exiting!\n");

		string packet=UPDATE_SP;
		packet+=this->successor().tostring()+"\n";
		packet+=DONT_CARE;
		this->send_to_pred(packet);

		packet=UPDATE_SP;
		packet+=DONT_CARE;
		packet+=this->predecessor().tostring();
		this->send_to_succ(packet);

		printf("updated successors and predecessors graph\n");


		std::vector<maptype> v=getfilesbetween(-INF,INF );
		string change_data=UPDATE_DATA;
		for (int i = 0; i < v.size(); ++i){
			if(!(v[i].second==node_addr)){
				change_data+="+ "+ maptostring(v[i])+" "+ datanames[v[i].first] +"\n";
				remove_file(v[i].first);
			}
		}
		
		if(change_data!=UPDATE_DATA)
		{
			if(change_data[change_data.size()-1]=='\n')
				change_data=change_data.substr(0,change_data.size()-1);
			send_to_succ(change_data);
		}
		
		printf("removed files\n");

		printf("Cleaning up...\n");

		packet=DEL_NODE;
		packet+=node_addr.tostring()+"\n";
		packet+=succ.tostring()+"\n";
		packet+=fulladdress(cser.serv_addr.sin_addr.s_addr,cser.getport()).tostring();

		send_to_pred(packet);

		
		kill(cpid, SIGABRT);
		exit(0);
	}


	void del_node(string s)
	{
		strings v=split(s, "\n");
		fulladdress n=fulladdress::fromstring(v[0]);
		fulladdress ns=fulladdress::fromstring(v[1]);
		fulladdress nc=fulladdress::fromstring(v[2]);
		//update finger table
		for (int i = 0; i < 5; ++i){
			if(fingertable[i]==n)
				fingertable[i]=ns;
		}

		//update data
		map<int,fulladdress>::iterator it=data.begin();
		std::vector<maptype> v2;
		for(;it!=data.end();it++)
		{
			if(it->second==nc)
				v2.push_back(*it);
		}

		for (int i = 0; i < v2.size(); ++i){
			// data.erase(v2[i].first);
			remove_file(v2[i].first);
		}
		if(!(node_addr==ns))
			send_to_pred(DEL_NODE+ s);
	}
	
	void get_files(string s)
	{
		strings p=split(s, "\n");
		if(p[0]==node_addr.tostring())
		{
			printf("-------Files present------\n");
			for (int i = 1; i < p.size(); ++i){
				printf("%s(%llu)\n", p[i].c_str(),consistent_hash(p[i]));
			}

			std::vector<maptype> v=getfilesbetween(-INF, INF);
			for (int i = 0; i < v.size(); ++i){
				printf("%s(%d)\n", datanames[v[i].first].c_str(),v[i].first);
			}
		}
		else{
			s=GET_FILES+s;
			if(s[s.size()-1]!='\n')
				s+="\n";

			std::vector<maptype> v=getfilesbetween(-INF, INF);
			for (int i = 0; i < v.size(); ++i){
				s+=datanames[v[i].first]+"\n";
			}
			if(s[s.size()-1]=='\n')
				s=s.substr(0,s.size()-1);
			send_to_succ(s);
		}
	}
};

node* nptr;
void node_gone(int num)
{	
	if(nptr->is_main_node())
	{
		printf("KILLALL sent\n");
		nptr->send_to_succ(KILLALL);
		kill(nptr->cpid, SIGTERM);
		exit(0);
	}
	else
	{
		nptr->delete_self();
	}
}

int main(int argc, char const *argv[])
{
	srand(time(NULL));
	
	signal(SIGINT, node_gone);

	int port=rand()%10000+10000;
	string addr;
	addr=string("127.0.0.1:")+port;
	if(argc>1)
		addr=string(MAIN_NODE_IP)+":"+MAIN_NODE_PORT;
	node n(addr);
	nptr=&n;
	n.init();

	return 0;
}



