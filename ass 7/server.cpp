#include "common.h"


using namespace std;

class node
{
private:
	fulladdress node_addr;
	unsigned long long node_num;
	fulladdress succ;
	fulladdress predec;
	Server ser;
	CopyServer cser;
	string filename;
public:
	std::map<int, fulladdress> data;

	node(std::string addr)
		:ser(SOCK_DGRAM,fulladdress::fromstring(addr).port),
			cser(SOCK_STREAM,10000+rand()%10000)
	{
		node_num = consistent_hash(addr.c_str());
		node_addr=fulladdress::fromstring(addr);
		printf("server at %s\n",  fulladdress(ser.serv_addr.sin_addr.s_addr,ser.getport()).tostring().c_str() );
		cser.start_listening(10);
		if(fork()==0)
		{
			printf("CopyServer started on port no %d\n",cser.getport() );
			cser.server_process();
			exit(0);
		}
	}

	void print_info()
	{
		printf("ID=%llu   predecessor=%llu   succ= %llu\n", node_num,consistent_hash(predec.tostring()),
						consistent_hash(succ.tostring()));

		printf("file ids\n");
		map<int,fulladdress>::iterator it=data.begin();
		for(;it!=data.end();it++)
			printf("%d\n",it->first );

		// printf("FINGER_TABLE\n");
		// for (int i = 0; i < 5; ++i){
		// 	printf("%d (%llu) --> %llu\n",i,((1<<i)+node_num)%HASH_SPACE, 
		// 					consistent_hash(fingertable[i].tostring()));
		// }


	}

	int issuccessorof(int id)
	{
		int succ_id=consistent_hash(succ.tostring().c_str());
		int pred_id=consistent_hash(predec.tostring().c_str());
		if(node_num>pred_id){
			if(id<=node_num && id>pred_id)
				return 1;
		}
		else{
			if(id>pred_id)
				return 1;
			else if(id<=node_num)
				return 1;
		}
		return 0;

	}

	void init(string dir=".")
	{
		this->succ=this->predec=this->node_addr;

		if(this->node_addr.tostring()!=string(MAIN_NODE_IP)+":"+MAIN_NODE_PORT)
			while(1){
				send_to_root(NEW_JOIN+node_addr.tostring());
				sockaddr_in si;
				socklen_t l=sizeof(si);

				string update_packet=ser.receive_data((sockaddr*)&si, &l);


				
				
				std::vector<string> v=split(update_packet, "\n");
				if(v[0]+"\n"!=UPDATE_SP)
				{
					if(v[0]+"\n"==KILL_YOURSELF)
					{
						printf("Sorry node name already exists\n");
						exit(0);
					}
				}
				else{
					set_s_p(fulladdress::fromstring(v[2]), fulladdress::fromstring(v[1]));
					
					printf("id %llu pred %llu succ %llu\n",node_num,consistent_hash(predec.tostring().c_str())
						,consistent_hash(succ.tostring().c_str()));

					std::vector<string> v=getfiles(dir);

					string forward=UPDATE_DATA;
					for (int i = 0; i < v.size(); ++i){

						maptype m;
						m.first=consistent_hash(v[i].c_str());
						m.second= fulladdress(cser.serv_addr.sin_addr.s_addr,cser.getport());
						forward+="+ "+maptostring(m)+"\n";
					}
					send_to_succ(forward);
					break;
				}
			}
		else
			printf("Root node started on %s\n",node_addr.tostring().c_str() );
		while(1)
		{
			printf("Enter filename\n");
			fd_set masterr;
			FD_ZERO(&masterr);
			
			FD_SET(ser.sockfd, &masterr);
			FD_SET(STDIN_FILENO, &masterr);

			if(select(ser.sockfd+1, &masterr, NULL, NULL, NULL)<0)
				error("select:");

			if(FD_ISSET(STDIN_FILENO, &masterr)){
				char word[100];
				scanf(" %s",word);
				
				filename=word;

				if(filename=="ls")
				{
					print_info();
					continue;
				}
				printf("searching for %s\n",word );
				int id=consistent_hash(word);
				
				if(data.find(id)!=data.end()){
					searchresult(data[id].tostring());
				}
				else{
					string packet=SEARCH;
					packet=packet+id+" "+node_addr.tostring();
					send_to_succ(packet);
				}
			}

			if(FD_ISSET(ser.sockfd, &masterr)){
				socklen_t len;
				sockaddr_in ad;
				string packet=ser.receive_data((sockaddr*)&ad, &len);
				
				pss p=getheader(packet);
				
				string header=p.first,tail=p.second;
				// printf("header=%s,tail=%s\n",header.c_str(),tail.c_str() );
				header+="\n";
				if(header==SEARCH)
					this->search(tail);
				if(header==NEW_JOIN)
					this->new_join(tail);
				if(header==UPDATE_DATA)
					this->update_data(tail);
				if(header==UPDATE_SP)
					updatesp(tail);
				if(header==SEARCH_RESULT)
					searchresult(tail);
				if(header==KILL_YOURSELF){
					printf("Sorry node name already exists\n");
					exit(0);
				}
			}



		}

	}

	void set_s_p(fulladdress pred,fulladdress suc){
		succ = suc;
		predec = pred;
	}
	fulladdress get_node_addr(){
		return node_addr;
	}
	unsigned long long get_node_num(){
		return node_num;
	}
	fulladdress successor(){
		return succ;
	}
	fulladdress predecessor(){
		return (predec);
	}
	int is_main_node()
	{
		std::string str(MAIN_NODE_IP);
		str += ":";
		str += MAIN_NODE_PORT;
		if(node_addr.ip==str)
			return 1;
		return 0;
	}
	void add_file(int fileid,fulladdress f)
	{
		printf("added %d to index\n", fileid);
		data[fileid]=f;
	}
	void remove_file(int fileid)
	{
		printf("removed file %d\n",fileid );
		data.erase(fileid);
	}
	//init inclusive
	std::vector<pair<int,fulladdress> > getfilesbetween(int init,int final)
	{
		std::vector<pair<int, fulladdress> > v;

		std::map<int, fulladdress>::iterator it=data.begin();

		for(;it!=data.end();it++){
			// if(it->first>=init && it->first<final)
			v.push_back(*it);
			printf("pushed %d\n",it->first );
		}
		return v;
	}
	void send_to_succ(string s)
	{
		printf("forwarding to successor %llu\n",consistent_hash(succ.tostring().c_str() ));
		sockaddr_in sadd=succ.tosockaddr_in();
		socklen_t len=sizeof(sadd);
		// sleep(2);
		ser.send_data(s, (sockaddr*)&sadd, len);
		
	}
	void send_to_pred(string s)
	{
		printf("forwarding to predecessor %llu\n",consistent_hash(predec.tostring().c_str() ));
		sockaddr_in sadd=predec.tosockaddr_in();
		socklen_t len=sizeof(sadd);
		// sleep(2);
		ser.send_data(s, (sockaddr*)&sadd, len);
		
	}
	void send_to_root(string s)
	{
		fulladdress root(MAIN_NODE_IP,atoi(MAIN_NODE_PORT));
		sockaddr_in sadd=root.tosockaddr_in();
		socklen_t len=sizeof(sadd);
		// sleep(2);
		printf("sent data to root %s %d\n",s.c_str(),ser.send_data(s, (sockaddr*)&sadd, len));
		
	}

	void new_join(string addr)
	{
		
		int id=consistent_hash(addr.c_str());
		printf("new_join packet arrived with id=%d\n",id);

		int pred_id=consistent_hash(predec.tostring().c_str());
		int succ_id=consistent_hash(succ.tostring().c_str());
		
		if(id==node_num){
			printf("Dropping packet with same id\n");
			string packet=KILL_YOURSELF;
			socklen_t l=sizeof(sockaddr_in);
			sockaddr_in s= fulladdress::fromstring(addr).tosockaddr_in();

			ser.send_data(packet,  (sockaddr*)&s, l);
			return;
		}

		if(pred_id==node_num&&succ_id==node_num&&
			node_addr.tostring()==MAIN_NODE_IP+string(":")+ MAIN_NODE_PORT)
		{
			//this is root
			printf("I'm root\n");
			this->predec=fulladdress::fromstring(addr);
			this->succ=fulladdress::fromstring(addr);
			string update=UPDATE_SP;
			update+=node_addr.tostring()+"\n";
			update+=node_addr.tostring();
			send_to_succ(update);
		}

		else if( (node_num>pred_id && id<node_num && id>pred_id) ||
			(node_num<pred_id && (id>pred_id ||  id< node_num  ) ))
		{
			//valid id for becoming its predecessor
			
			/*
			send to actual predecessor about updating predessor
			*/
			string update=UPDATE_SP;
			update+=addr+"\n";
			update+=DONT_CARE;
			send_to_pred(update);
			
			/*
			send to updated predecessor about its both 
			successor and predecessor
			*/
			string prev_pred=predec.tostring();
			this->predec=fulladdress::fromstring(addr);

			update=UPDATE_SP;
			update+=node_addr.tostring()+"\n";
			update+=prev_pred;
			send_to_pred(update);


			/*
				send fids to the new predecessor
			*/
			std::vector<maptype> v=getfilesbetween(-INF,INF );
			string change_data=UPDATE_DATA;
			printf("V.size=%lu\n",v.size() );
			for (int i = 0; i < v.size(); ++i){
				if(!issuccessorof(v[i].first)){
					change_data+="+ "+ maptostring(v[i])+"\n";
					remove_file(v[i].first);	
				}
			}
			
			if(change_data!=UPDATE_DATA)
			{
				if(change_data[change_data.size()-1]=='\n')
					change_data=change_data.substr(0,change_data.size()-1);
				send_to_pred(change_data);
			}
				
		}
		else{
			string s;
			s=NEW_JOIN+addr;
			send_to_succ(s);
		}
	}

	string update_data(string s)
	{
		printf("update_data packet arrived with %s\n",s.c_str());
		int succ_id=consistent_hash(succ.tostring().c_str());
		int pred_id=consistent_hash(predec.tostring().c_str());
		string forward=UPDATE_DATA;


		std::vector<string> v=split(s, "\n");

		
		for (int i = 0; i < v.size(); ++i){
			
			std::vector<string> v2=split(v[i], " ");
			
			int id=atoi(v2[1].c_str());
			if(v2[0]=="+"){

				fulladdress ad=fulladdress::fromstring(v2[2]);	
				if(node_num>pred_id){
					if(id<=node_num && id>pred_id)
						add_file(id, ad);
					else
						forward+=v[i]+"\n";
				}
				else{
					if(id>pred_id)
						add_file(id,ad );
					else if(id<=node_num)
						add_file(id, ad);
					else
						forward+=v[i]+"\n";
				}
			}

			else{
				if(data.find(id)!=data.end())
					data.erase(id);
				else
					forward+=v[i]+"\n";
			}
		
		}
		if(forward!=UPDATE_DATA)
			send_to_succ(forward);
		return forward;
	}

	void search(string s)
	{
		printf("search packet arrived with %s\n",s.c_str());
		std::vector<string> v=split(s, " ");
		int id=atoi(v[0].c_str());

		fulladdress dest=fulladdress::fromstring(v[1]);
		if(dest.tostring()==node_addr.tostring())
		{
			printf("file not found!!!\n");
			return;
		}
		if(data.find(id)!=data.end()){
			string packet=SEARCH_RESULT;
			packet+=data[id].tostring();

			sockaddr_in addr=dest.tosockaddr_in();
			socklen_t len=sizeof(addr);
			ser.send_data(packet, (sockaddr*)&addr,len);
		}
		else
			send_to_succ(SEARCH+ s);

	}
	void updatesp(string s)
	{
		
		std::vector<string> v=split(s, "\n");

		if(v[0]+"\n"!=DONT_CARE){
			this->succ=fulladdress::fromstring(v[0]);
			printf("updated successor as %llu\n",consistent_hash(v[0].c_str()) );
		}
		if(v[1]+"\n"!=DONT_CARE){
			this->predec=fulladdress::fromstring(v[1]);
			printf("updated predecessor as %llu\n",consistent_hash(v[1].c_str()) );
		}
	}

	void searchresult(string s)
	{
		printf("searchresult packet arrived with %s\n",s.c_str());
		fulladdress f=fulladdress::fromstring(s);
		
			Client c(SOCK_STREAM,f.port,f.ip.c_str());
			c.connect_toserver();
			c.send_data(filename);
			c.read_data_ncopy(filename);
			// exit(0);
		
		return;
	}
};

node* nptr;
void node_gone(int num)
{

}

int main(int argc, char const *argv[])
{
	srand(time(NULL));
	
	signal(SIGINT, node_gone);

	int port=rand()%10000+10000;
	string addr;
	addr=string("127.0.0.1:")+port;
	if(argc>1)
		addr=string(MAIN_NODE_IP)+":"+MAIN_NODE_PORT;
	node n(addr);
	n.init();
	nptr=&n;
	return 0;
}



