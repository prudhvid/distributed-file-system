
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <bits/stdc++.h>
#include "dirent.h"
#include <unistd.h>
#include "fcntl.h"

#define TRUE 1
#define FALSE 0
#define EQUAL(X,Y) (strcmp(X, Y)==0?1:0)

using std::string;
void error(const char* msg)
{
	perror(msg);
	exit(1);
}

std::vector<std::string> getfiles(std::string directory)
{
	
	std::vector<std::string> v;
	DIR* dp;
	struct dirent* ep;
	dp=opendir(directory.c_str());

	if(dp!=NULL)
	{
		while((ep=readdir(dp))){
			if(!EQUAL(ep->d_name,"..") && !EQUAL(ep->d_name,".")&&ep->d_name[0]!='.')
				v.push_back(ep->d_name );
		}
		closedir(dp);
	}
	else 
		error("could not open Directory");
	return v;
}

class Client{
	
public:
	struct sockaddr_in myaddr, serv_addr;
	int fd, i, slen;
	char buf[10000];	/* message buffer */
	int recvlen;
	char *server_name;
	int portno,type;
	Client(int socktype,int port,std::string servername)
	{
		/* # bytes in acknowledgement message */
		slen=sizeof(serv_addr);
		type=socktype;
		/* create a socket */

		if ((fd=socket(AF_INET, socktype, 0))==-1)
			printf("socket created\n");

		/* bind it to all local addresses and pick any port number */

		memset((char *)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		myaddr.sin_port = htons(0);

		if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
			perror("bind failed");
		}


		memset((char *) &serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		if (inet_aton(servername.c_str(), &serv_addr.sin_addr)!=0) {
			inet_pton(AF_INET, servername.c_str(), &serv_addr.sin_addr);
		}
		server_name=strdup(servername.c_str());
		portno=port;
		
	}
	Client(int socktype,int port,in_addr_t addr)
	{
		slen=sizeof(serv_addr);
		type=socktype;
		/* create a socket */

		if ((fd=socket(AF_INET, socktype, 0))==-1)
			printf("socket created\n");

		/* bind it to all local addresses and pick any port number */

		memset((char *)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		myaddr.sin_port = htons(0);

		if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
			perror("bind failed");
		}


		memset((char *) &serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		serv_addr.sin_addr.s_addr=addr;
		portno=port;
	}
	int send_data(std::string data)
	{

		// printf("Sending packet %d to %s port %d\n", i, server_name, portno);
		
		int res;
		if(type==SOCK_DGRAM){
			if ((res=sendto(fd, data.c_str(), data.size()+1, 
							0, (struct sockaddr *)&serv_addr, slen))==-1) {
				error("sendto");
			}
		}
		else{
			if((res=send(fd, data.c_str(), data.size()+1, 0))<0)
				error("send_data:");
		}
		return res;
	}
	std::string receive_data()
	{
		int nbytes;
		socklen_t leng=sizeof(serv_addr);
		if(type==SOCK_DGRAM)
			nbytes = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&serv_addr, &leng);
		else
			nbytes=recv(fd, buf, sizeof(buf), 0);
		if (nbytes<0)
		{
			error("receive_data:");
			sprintf(buf, "0");
		}
		return std::string(buf);
	}
	std::string read_data_ncopy(string name)
	{
		int n_chars;
		int wfd=creat(name.c_str(), S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
		if(wfd>0)
			printf("file openend successfully\n");
		while( (n_chars = read(fd, buf, sizeof(buf))) > 0 )
	  	{
	  		buf[n_chars+1]='\0';
	  		if(string(buf)=="ENDOFFILE")
	  			break;
			if( write(wfd, buf, n_chars) != n_chars )
			{
			  printf("error writing\n");
			}
		 
		 
			if( n_chars == -1 )
			{
			  printf("error reading\n");
			}
	  	}
	  	close(wfd);
	  	printf("written successfully to %s\n",name.c_str());
	  	return string(buf);
	}
	int connect_toserver()
	{
		int res=connect(fd, (sockaddr*)&serv_addr, slen);
		if(res<0)
			error("connect_toserver:");
		printf("connected to server at %u\n",serv_addr.sin_addr.s_addr);
		return res;
	}
	
};




class CopyServer{
	
public:
	int sockfd,newfd,newsockfd,type;
	sockaddr_in serv_addr,cli_addr;
	socklen_t clilen;
	std::map<std::string, in_addr_t> database;
	char buff[100];
	CopyServer(int socktype,int port)
	{
		
		socklen_t clilen;
		char buffer[256];
		
		int n;

		sockfd = socket(AF_INET, socktype, 0);
		if (sockfd < 0) 
			error("ERROR opening socket");
		type=socktype;

		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(port);
		if (bind(sockfd, (struct sockaddr *) &serv_addr,
								sizeof(serv_addr)) < 0) 
			error("ERROR on binding");
		
	}
	int start_listening(int waitno)
	{
		listen(sockfd, waitno);
		clilen = sizeof(cli_addr);
     	return 0;
	}
	int accept_new()
	{
		if(type==SOCK_DGRAM)
			return 0;
		newsockfd=accept(sockfd, (sockaddr*)&cli_addr, &clilen);
		if(newsockfd<0)
			error("accept:");
		return newsockfd;
	}
	/*for sock_stream*/
	int receive_data()
	{
		int res=recv(newsockfd, buff, sizeof(buff), 0);
		if(res<0)
			error("receive_data:");
		return res;
	}
	/*for datagram*/
	int receive_data(sockaddr* addr,socklen_t* len)
	{
		int nbytes=recvfrom(newsockfd, buff, sizeof(buff), 0,
					(sockaddr*)&cli_addr,len);
		if(nbytes<0)
			error("receive_data(int):");
		return nbytes;
	}
	/*for sock stream*/
	int send_data(std::string data)
	{
		int res=send(newsockfd, data.c_str(), data.size()+1, 0);
		if(res<0)
			error("send_data:");
		return res;
	}
	/*for datagram*/
	int send_data(std::string data,sockaddr* addr,socklen_t &len)
	{
		int res=sendto(newsockfd,data.c_str(), data.size()+1, 0, 
				addr, len);
			if(res<0)
				error("send_data(int):");
		return res;
	}
	void server_process()
	{
		while(1){
			printf("SERVER-waiting for users\n");
			accept_new();
			printf("SERVER-new user arrived!!\n");
			receive_data();
			printf("SERVER-client asked for file %s!!\n",buff);
			std::string filename(buff);
			std::vector<std::string> v=getfiles("./");
			int i;
			for (i = 0; i < v.size(); ++i){
				if(v[i]==filename)
					break;
			}
			if(i==v.size())
				printf("SERVER-file not found\n");
			else
			{
				//send file
				int n_chars;
				int readfd=open(filename.c_str(), O_RDONLY);
				while( (n_chars = read(readfd, buff, sizeof(buff))) > 0 )
			  	{
					if( write(newsockfd, buff, n_chars) != n_chars )
					{
					  printf("SERVER-error writing\n");
					}
				 
				 
					if( n_chars == -1 )
					{
					  printf("SERVER-error reading\n");
					}
			  	}
			  	close(readfd);
			  	// sleep(3);
			  	// sprintf(buff, "ENDOFFILE");
			  	// write(newsockfd, buff,strlen(buff)+1);
			  	// printf("SERVER-written successfully\n");
			  	close(newsockfd);
			}
		}
	}
};

int main(int argc, char const *argv[])
{
	std::string name="localhost",dir="./";
	if(argc>1)
		name=argv[1];
	if(argc>2)
		dir=argv[2];

	Client c(SOCK_DGRAM,10500,name);

	std::vector<std::string> v=getfiles(dir);
	std::string buff;
	for (int i = 0; i < v.size(); ++i){
		buff=buff+v[i]+"\n";
	}
	
	
	
	

	CopyServer s(SOCK_STREAM,10800);
	s.start_listening(10);
	if(fork()==0)
	{
		s.server_process();
		exit(0);
	}
	sleep(2);
	c.send_data( std::string("0\n")+buff );

	while(1)
	{
		printf("\nEnter filename \n");
		char file[30];
		scanf("%s",file);

		printf("sending filename %s to server",file);
		c.send_data(std::string("1\n")+std::string(file) );

		std::string address=c.receive_data();

		printf("file is present at address %s\n",address.c_str() );

		if(address=="-1")
		{
			printf("file not found\n");
			continue;
		}
		Client c2(SOCK_STREAM,10800,atoi(address.c_str()));
		c2.connect_toserver();
		c2.send_data(file);
		c2.read_data_ncopy(file);
	}
	wait(0);
	return 0;
}