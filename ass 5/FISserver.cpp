#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <bits/stdc++.h>

void error(const char* msg)
{
	perror(msg);
	exit(1);
}
void sigchld_handler(int s)
{
	while(waitpid(-1, NULL, WNOHANG) > 0);
}
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


class Server{
	
public:
	int sockfd,newfd,newsockfd,type;
	sockaddr_in serv_addr,cli_addr;
	socklen_t clilen;
	std::map<std::string, in_addr_t> database;
	char buff[100];
	Server(int socktype,int port)
	{
		
		socklen_t clilen;
		char buffer[256];
		
		int n;

		sockfd = socket(AF_INET, socktype, 0);
		if (sockfd < 0) 
			error("ERROR opening socket");
		type=socktype;

		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(port);
		if (bind(sockfd, (struct sockaddr *) &serv_addr,
								sizeof(serv_addr)) < 0) 
			error("ERROR on binding");
		
	}
	int start_listening(int waitno)
	{
		listen(sockfd, waitno);
		clilen = sizeof(cli_addr);
     	return 0;
	}
	int accept_new()
	{
		if(type==SOCK_DGRAM)
			return 0;
		newsockfd=accept(sockfd, (sockaddr*)&cli_addr, &clilen);
		if(newsockfd<0)
			error("accept:");
		return newsockfd;
	}
	/*for sock_stream*/
	int receive_data()
	{
		int res=recv(sockfd, buff, sizeof(buff), 0);
		if(res<0)
			error("receive_data:");
		return res;
	}
	/*for datagram*/
	int receive_data(sockaddr* addr,socklen_t* len)
	{
		int nbytes=recvfrom(sockfd, buff, sizeof(buff), 0,
					(sockaddr*)&cli_addr,len);
		if(nbytes<0)
			error("receive_data(int):");
		return nbytes;
	}
	/*for sock stream*/
	int send_data(std::string data)
	{
		int res=send(sockfd, data.c_str(), data.size()+1, 0);
		if(res<0)
			error("send_data:");
		return res;
	}
	/*for datagram*/
	int send_data(std::string data,sockaddr* addr,socklen_t &len)
	{
		int res=sendto(sockfd,data.c_str(), data.size()+1, 0, 
				addr, len);
			if(res<0)
				error("send_data(int):");
		return res;
	}
	void server_process()
	{
		char buff[1000];
		int nbytes=recvfrom(sockfd, buff, sizeof(buff), 0,
					(sockaddr*)&cli_addr,&clilen);
		if(nbytes<0)
			error("server_process:");
		
		
		char* file;
		int i=0;
		file=strtok(buff, "\n");
		if(atoi(file)==0){
			// printf("client %u  has files %s\n",cli_addr.sin_addr.s_addr,buff );	
			while(file!=NULL)
			{
				file=strtok(NULL, "\n");
				if(file){
					database[file]=cli_addr.sin_addr.s_addr;
					printf("added %s to database\n",file );
				}
			}
		}
		else
		{
			file=strtok(NULL, "\n");
			printf("searching for %s\n", file);

			
			if(database.find(file)!=database.end())
			{
				sprintf(buff, "%u",database[file]);
				printf("found file at %s\n", buff);
			}
				
			else
			{
				printf("not found\n");
				sprintf(buff, "-1");
			}
			int res=sendto(sockfd,buff, strlen(buff)+1, 0, 
				(sockaddr*)&cli_addr, clilen);
			
			
			if(res<0)
				error("sending:");
		}

	}
};



int main(int argc, char const *argv[])
{
	Server s(SOCK_DGRAM,10500);

	s.start_listening(2);

	while(1)
	{
		s.accept_new();
		int pid;
		// if((pid=fork())==0)
		// {
			s.server_process();
			// exit(0);
		// }
		// close(s.newsockfd);
		// wait(0);
	}

	return 0;
}